<?php

class mySQLdatabase
{
	private $connection;
	
	function __construct()
	{
		$this->openConnection();
	}

	public function openConnection()
	{
		$this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD);
		if (!$this->connection)
		{
			die("Database connection failed: " . mysql_error());
		}
		else
		{
			$dbSelect = mysql_select_db(DB_NAME, $this->connection);
			if(!$dbSelect)
			{
				die("Database connection failed: " . mysql_error());
			}
		}
	}
	
	public function closeConnection()
	{
		if(isset($this->connection))
		{
			mysql_close($this->connection);
			unset($this->connection);
		}
	}
	
	public function query($sql)
	{
		$result = mysql_query($sql, $this->connection);
		$this->confirmQuery($result);
		return $result;
	}
	
	public function getPage($page, $table)
	{
		$sql = "SELECT * FROM ".$table." WHERE page_name = '".$page."'";
		$resultSet = $this->query($sql);
		return mysql_fetch_array($resultSet);
	}
	
	public function getSubPage($subPage, $table)
	{
		$sql = "SELECT * FROM ".$table." WHERE page_name = '".$subPage."'";
		$resultSet = $this->query($sql);
		return mysql_fetch_array($resultSet);
	}
	
	public function getAdminMenu($sql)
	{
		$resultSet = $this->query($sql);
		while ($row = mysql_fetch_array($resultSet, MYSQL_NUM))
		{
		    $menu .= GlobalFunctions::buildAdminLink($row[0]);  
		}
		return $menu;
	}
	
	public function getClientTable($sql)
	{
		$resultSet = $this->query($sql);
		$menu = '<table>';
		$i = 0;
		while ($row = mysql_fetch_array($resultSet, MYSQL_NUM))
		{	
			if($i>1){$i=0;}
		    $menu .= globalFunctions::BuildTableRow($row[2], $row[3], $row[4], $i);
		    $i++;  
		}
		$menu.='</table>';
		return $menu;
	}
	
	private function confirmQuery($result)
	{
		if(!$result)
		{
			die("Database connection failed: " . mysql_error());
		}
	}
	
	public function getGalleryInformation($gallery)
	{
		
		$stringGallery = mysql_real_escape_string(trim($gallery));		
		$sql = "SELECT * FROM picture WHERE picture_gallery='".$stringGallery."'";
		$resultSet = $this->query($sql);
		$listImages = array();
		while ($row = mysql_fetch_array($resultSet))
		{
		   array_push($listImages,array($row['picture_id'],$row['picture_gallery'],$row['picture_name'],$row['picture_thumb_name'],$row['picture_description']));
		}
		return $listImages;
	}
}

$database = new mySQLdatabase();

?>