<?php
define('CMSDIR', '__.simplicms/');
switch($local_server)
{
	case 'local' :
		// LOCAL
		define('DB_SERVER', 'localhost');
		define('DB_USER', 'root');
		define('DB_PASSWORD', 'root');
		define('DB_NAME', 'designat_villageframegallery');
		break;
	case 'server' :
		//DATABASE CONFIGURATIONS
		define('DB_SERVER', 'localhost');
		define('DB_USER', 'designat_allsql');
		define('DB_PASSWORD', 'candyCanes');
		define('DB_NAME', 'designat_villageframegallery');
		break;
}


//SMTP CONFIGURATION FOR MAIL CLIENT
define('SMTP_SERVER', 'smtp.gmail.com');
define('SMTP_PORT', 465);
define('SMTP_SSL',1);
define('SMTP_USERNAME', 'no-reply@designatwork.net');
define('SMTP_PASSWORD', 'candyCanes');

// INCLUDES
require SITE_ROOT.CMSDIR.'__.incld/global.functions.inc.php';
require SITE_ROOT.CMSDIR.'__.incld/global.navigation.inc.php';
require SITE_ROOT.CMSDIR.'__.incld/db.query.inc.php';
// LIBRARIES
require SITE_ROOT.CMSDIR.'__.incld/form.processor.inc.php';
require SITE_ROOT.CMSDIR.'__.incld/lib/swift_required.php';
require SITE_ROOT.CMSDIR.'__.incld/lib/mail_chimp/MCAPI.class.php';
// SITE FRAMEWORK
require SITE_ROOT.CMSDIR.'__.frmwrk/base.frmwrk.php';
// PAGE TEMPLATES
require SITE_ROOT.CMSDIR.'__.pgs/common.pgs.php';
require SITE_ROOT.CMSDIR.'__.pgs/gallery.pgs.php';
?>