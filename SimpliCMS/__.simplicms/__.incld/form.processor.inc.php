<?php

class formProcessor
{
	private $formVars;
	private $formContent;
	private $sendToEmail;
	private $subject;
	private $resume;
	private $resumeData;
	private $redirect;
	
	function __construct()
	{
		
	}
	
	public function processForm($swiftMail, $mailer, $contactList, $sbj, $sendto='notset')
	{
		$this->formVars = $contactList;
		$sendto = $sendto == 'notset' ? 'fredy@designatwork.com' : $sendto;
		$this->redirect = $redirect;
		//
		$swiftMail->setSubject($sbj);
		$swiftMail->setFrom(array('fredy@designatwork.com'));
		$swiftMail->setBody($this->loopEmailContent(), 'text/html');
		$swiftMail->setTo(array(('fredy@designatwork.com')));		
		$result = $mailer->send($swiftMail);
		$this->redirectToThanks();
	}
	
	public function processFormWithFile($swiftMail, $mailer, $contactList, $sbj, $resumeFile, $sendto='notset')
	{
		$this->formVars = $contactList;
		$sendto = $sendto == 'notset' ? 'fredy@designatwork.com' : $sendto;
		//
		$swiftMail->setSubject($sbj);
		$swiftMail->setFrom(array($contactList['Email:']));
		$swiftMail->setBody($this->loopEmailContent(), 'text/html');
		$swiftMail->setTo(array(($sendto)));
		$swiftMail->attach($resumeFile);
		$result = $mailer->send($swiftMail);
		$this->redirectToThanks();
	}
	
	private function loopEmailContent()
	{
		$formContent = '
		<html>
		<head>
		  <style>
		  	body{font-size:12px; font-family:Arial;}
		  	.label {font-weight:bold;}
		  	td{padding-bottom:3px; margin-bottom:3px; border-bottom:1px solid #dadada }
		  </style>
		</head>
		<body>
			<table>
		';
		foreach($this->formVars as $key => $value)
		{
			$formContent.='<tr><td class="label" width="120" valign="top">'.$key.'</td><td>'.$value.'</td></tr>';
		}
		$formContent.='
			</table>
		</body>
		</html>
		';
		return $formContent;
	}
	
	private function redirectToThanks()
	{
		header('Location: /comments-thank-you');
	}
	
}

$formProcess = new formProcessor();

?>