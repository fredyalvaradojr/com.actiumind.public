<?php

class globalNavigation
{
	public $navigation;
	public $subNavigation;
	public $loggedinNavigation;
	public $loggedinSubNavigation;
	
	function __construct()
	{
		// sets common navigation
		$this->navigation = '
		<nav id="main_menu">
			<ul>
			    <li><a href="/" id="home">Home</a></li>
				<li><a href="/about-us" id="about_us">About Us</a></li>
				<li id="framing_menu">
					<a href="/framing" id="framing">Framing</a>
					<ul class="sub_menu">
						<li><a href="/framing/archival-materials" id="archival_materials">Archival Materials</a></li>
						<li><a href="/framing/types-of-glass" id="types_of_glass">Types of Glass</a></li>
					</ul>
				</li>
				<li><a href="/delivery-and-installation" id="delivery_and_installation">delivery &<br>Installation</a></li>
				<li><a href="/art" id="art">art</a></li>
				<li id="gifts_menu">
					<a href="/gifts" id="gifts">gifts</a>
					<ul class="sub_menu">
						<li><a href="/gifts/photo-frames" id="photo_frames">photo frames</a></li>
					</ul>
				</li>
				<li><a href="/photo-restoration" id="photo_restoration">Photo restoration</a></li>
				<li id="last_menu_item"><a href="/contact-us" id="contact_us">Contact Us</a></li>
			</ul>
		</nav>
		';
	}
	
	public function getNavigation($page)
	{
		$cleanNavigation = $this->navigation;
		$findId = 'id="'.$page.'"';
	    $activeClass = 'id="'.$page.'" class="active"';
	    
	    return str_replace($findId, $activeClass, $cleanNavigation);		
	}
	
	public function getActiveMenu($page, $parent)
	{
		return $pageClass = isset($parent) ? $parent : $page;
	}
}

?>