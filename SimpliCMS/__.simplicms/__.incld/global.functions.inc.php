<?php

class globalFunctions
{
	public static function permanentRedirect($page)
	{
		switch($page)
		{
			case 'water-softeners' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/products/water-softeners" );
				break;
			case 'water-heaters' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/products/water-heaters" );
				break;
			case 'employment-application' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/employment/employment-application" );
				break;
			case 'drain-cleaning' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/drain-cleaning" );
				break;
			case 'commercial-plumbing' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/commercial_plumbing_new_construction_and_remodeling" );
				break;
			case 'drain-water-rerouting' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/drain-water-rerouting" );
				break;
			case 'residential-service-repair' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/residential-service-repair" );
				break;
			case 'video-pipe-investigation' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/video-pipe-investigations" );
				break;
			case 'schedule-an-appointment' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/contact-us/schedule-an-appointment" );
				break;
			case 'commercial-service-repair' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/commercial-service-repair" );
				break;
			case 'water-energy-conservation-audits' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/services/water-energy-conservation-audits" );
				break;
			case 'reed-plumbing-changes-corporate-name' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://smartplumbinginc.com/news/reed-plumbing-changes-corporate-name" );
				break;
			case 'contact' :
				Header( "HTTP/1.1 301 Moved Permanently" ); 
				Header( "Location: http://www.smartplumbinginc.com/contact-us" );
				break;
			case '404':
				header("Location: /page-not-found");
				die();
				break;
		}
		
	}
	
	public static function explodeCurrentUrl()
	{
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80")
		{
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}
		else
		{
			$pageURL = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return explode("/", $pageURL);
	}
	
	public static function getCurrentUrl()
	{
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80")
		{
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}
		else
		{
			$pageURL = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	
	public static function cleanBreadCrumb($breadCrumb)
	{
		$cleanProduct = str_replace('-', ' ', $breadCrumb);
		$cleanProduct = ucwords($cleanProduct);
		$cleanProduct = str_replace('Pci', 'PCI', $cleanProduct );
		$cleanProduct = str_replace('Solutions Product Type', 'Solutions by Product Type', $cleanProduct );
		$cleanProduct = str_replace('PCI Main Concern', 'PCI', $cleanProduct );
		$cleanProduct = str_replace('Deployed Vpn', 'Deployed VPN', $cleanProduct );
		return $cleanProduct;
	}
	
	public static function cleanURLName($name)
	{
		return str_replace('-', "_", $name);
	}
	
	public static function createBreadCrumbs()
	{
		// get url check name and return needed one, check if part of url is empty if so, add end graphic if not add continuation graphic
		$urlPathArr = gFunct::getCurrentUrl();
		$breadCrumb = '<ul id="breadcrumb"><li><a href="/'.$urlPathArr[1].'">'.gFunct::cleanBreadCrumb($urlPathArr[1]).'</a></li>';
		if(isset($urlPathArr[2])) 
		{
			$breadCrumb .= '<li class="middle">mid</li><li><a href="/'.$urlPathArr[1].'/'.$urlPathArr[2].'">'.gFunct::cleanBreadCrumb($urlPathArr[2]).'</a></li>';
			if(isset($urlPathArr[3]))
			{ 
				$breadCrumb .= '<li class="middle"></li><li><a href="/'.$urlPathArr[1].'/'.$urlPathArr[2].'/'.$urlPathArr[3].'">'.gFunct::cleanBreadCrumb($urlPathArr[3]).'</a></li>'; 
				if(isset($urlPathArr[4]))
				{ 
					$breadCrumb .= '<li class="middle"></li><li><a href="/'.$urlPathArr[1].'/'.$urlPathArr[2].'/'.$urlPathArr[3].'/'.$urlPathArr[4].'">'.gFunct::cleanBreadCrumb($urlPathArr[4]).'</a></li><li class="end">end</li>'; 
					
				} 
					else
					{ $breadCrumb .= '<li class="end">end</li>'; }
			} 
				else
				{ $breadCrumb .= '<li class="end">end</li>'; }
		} 
		else 
		{ $breadCrumb .= '<li class="end">end</li>'; }
		$breadCrumb .= '</ul>';
		
		return $breadCrumb;
	}
	
	public static function buildAdminLink($link)
	{
		return '<li><a href="index.php?edit_page='.$link.'" rel="ajax">'.globalFunctions::cleanUnderscores($link).'</a></li>';
	}
	
	public static function cleanUnderscores($text)
	{
		return str_replace('_', " ", $text);
	}
	
	public static function cleanQueryInputs($text)
	{
		return str_replace('\'', "\'\'", $text);
	}
	
	public static function activeSubNavigation($menuItem, $currentPage, $subNav)
	{
		if($menuItem == $currentPage)
			return $subNav;
	}
	
	public static function BuildTableRow($projectName, $projectLocation, $projectWork, $i)
	{
		if($i<1) {$row='<tr class="highlight">';} else {$row='<tr>';}
		$row.='<td>'.$projectName.'</td><td>'.$projectLocation.'</td><td>'.$projectWork.'</td>';
		$row.='</tr>';
		return $row;
	}
	
	public static function checkUploadedResume($obj)
	{
		$allowedExtensions = array("txt","pdf","rtf","doc","docx"); 
		$extension = end(explode('.', strtolower($obj['name'])));
    	if ($obj['tmp_name'] > '')
    	{ 
     		if (!in_array(end(explode(".", strtolower($obj['name']))), $allowedExtensions))
     		{
     			header('Location: /contact-us/careers');
       			die(); 
    		} 
    	} 

	}
	
	
}

?>