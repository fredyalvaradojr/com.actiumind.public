<?

class baseFramework 
{
	public $pageVar = array();
	public $globalNav;
	public $database;
	
	function __construct()
	{
		// run init query to get templates initial properties
		$this->globalNav = new globalNavigation();
		
		$this->pageVar['google'] = 'UA-22203998-1 -';
		$this->pageVar['title'] = 'Welcome';
		$this->pageVar['page_keywords'] = '';
		$this->pageVar['page_description'] = '';
	}
	
	public function __set($variableName, $variableValue)
	{
		$this->pageVar[$variableName] = $variableValue;
	}
	
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->navigationArea();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
	
	public function htmlHead()
	{
		$htmlHead = '		
		<!DOCTYPE html>
		<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
		<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
		<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
		<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
		<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
		<head>
			<meta charset="UTF-8">
			
			<title>'.$this->pageVar['page_title'].' | Village Frame Gallery | Houston, TX</title>
			
			<meta name="keywords" content="'.$this->pageVar['page_keywords'].'">
			<meta name="description" content="'.$this->pageVar['page_description'].'">
			
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
			<meta name="viewport" content="width=1024">
			
			<link rel="shortcut icon" href="favicon.ico">
			
			<link rel="stylesheet" href="/assets/css/page.styles.css?v=1.0">
			
			'.$this->pageVar['page_extra_stylesheets'].'
			'.$this->pageVar['special_styles'].'
			
			<script src="/assets/js/libs/jquery-1.5.1.min.js"></script>
			<!--<script src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>-->
			<script src="/assets/js/libs/modernizr-1.7.min.js"></script>
			<script src="/assets/js/libs/cufon-yui.js"></script>
			<script src="/assets/js/libs/villagenist.font.js"></script>
			<script type="text/javascript"> 
				Cufon.replace(\'.villagenist\')(\'#main_menu a\',{hover: true});
			</script> 
			'.$this->pageVar['page_javascript'].'
		</head>
		';
		return $htmlHead;
	}
	
	public function startBodyWrap()
	{
		$startBodyWrap = '
		<body>
			<!-- start wrapper -->
			<div id="wrapper" class="'.$this->globalNav->getActiveMenu($this->pageVar['page_name'], $this->pageVar['page_subpage_name']).'">
			';
		return $startBodyWrap;
	}
	
	public function header()
	{
		$header = '
				<!-- start header -->
				<header class="clearfix">
		        	<a href="/" class="ir" id="header_logo">Village Frame Gallery | Enhance your everyday.</a>
		        	<ul id="header_information" class="villagenist">
		        		<li class="highlight" id="phone"><strong>713.528.2288</strong></li>
		        		<li>2708 Bissonnet Street  |  Houston, Texas 77005</li>
		        		<li class="highlight">FRAMING <sup><strong>.</strong></sup> ART <sup><strong>.</strong></sup> GIFTS</li>
		        	</ul>
		        </header>
				<!-- end header -->
	    ';
	    return $header;
	}
	
	public function navigationArea()
	{
		$navigation = '
				<!-- start of navigation -->
				<div id="navigation">
					<div id="nav_top"></div>
					<div id="nav_body" class="clearfix uc">
						<img src="/assets/img/page_header/'.$this->pageVar['page_name'].'.jpg" width="788" height="268" id="page_header">
						'.$this->globalNav->getNavigation($this->pageVar['page_name']).'
					</div>
					<div id="nav_bottom"></div>
		        </div>
		        <!-- end of navigation -->
		';
		return $navigation;
	}
	
	public function contentArea()
	{		
		$contentArea = '
				<!-- start of page content -->
				<div id="content" class="clearfix">
					
		            <section id="main_content">
		            	'.$this->pageVar['page_content'].'
		            </section>
		            <aside id="sub_content">
		            	'.$this->pageVar['page_subcontent'].'
		            </aside>
				</div>
				<!-- end of page content -->
		';
		return $contentArea;
	}	
	
	public function footer()
	{
		$footer = '
			<!-- start of footer -->
			<footer>&copy; Copyright 2011 Village Frame Gallery. All Rights Reserved. Website design by <a href="http://www.designatwork.com/">Design At Work</a>.</footer>
			<!-- end of footer -->
			</div>
			<!-- end wrapper -->
		';
		return $footer;
	}
	
	public function endBodyWrap()
	{
		$endBodyWrap = '		
		<!-- google analytics code -->
		<script type="text/javascript"> 
		var _gaq = _gaq || [];
		_gaq.push([\'_setAccount\', \''.$this->pageVar['google'].'\']);
		_gaq.push([\'_trackPageview\']);
		
		(function() {
		var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
		ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
		var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
		})();			 
		</script>
		<!-- end google analytics code -->
		<script type="text/javascript"> Cufon.now(); </script>
		</body>
		</html>
		';
		return $endBodyWrap;
	}
}

?>