<?

class adminBaseFramework 
{
	public $pageVar = array();
	public $_globalNavigation;
	
	function __construct()
	{
		// run init query to get templates initial properties	
		$this->pageVar['stylesheets'] = '
		<link rel="stylesheet" href="/assets/css/admin.styles.css">
		<!--[if lte IE 7]>
		<style>
			
		</style>
		<![endif]-->
		';
	}
	
	public function __set($variableName, $variableValue)
	{
		$this->pageVar[$variableName] = $variableValue;
	}
	
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
	
	public function htmlHead()
	{
		$htmlHead = '		
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta charset="utf-8" />
		<title>'.$this->pageVar['title'].'simplicms Editor</title>
		'.$this->pageVar['stylesheets'].'
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		
		</head>
		';
		return $htmlHead;
	}
	
	public function startBodyWrap()
	{
		$startBodyWrap = '
		<body>
			<!-- start wrapper -->
			<div id="wrapper">
			';
		return $startBodyWrap;
	}
	
	public function header()
	{
		$header = '
				<!-- start header -->
				<div id="header">
					<ul>
						<li><a href="/editor/">Add a Page</a></li>
					'.$this->pageVar['menu'].'					
					</ul>
				</div>
				<!-- end header -->
	    ';
	    return $header;
	}
	
	public function contentArea()
	{
		$contentArea = '
				<!-- start of page content -->
				<div id="page_content">
					<form action="/editor/index.php" method="post"> 
					<section id="content">
						<ul>
							<li><lable>title</lable><input type="text" name="page_title" value="'.$this->pageVar['page_title'].'"></li>
							<li><lable>page Name</lable><input type="text" name="page_name" value="'.$this->pageVar['page_name'].'"></li>
							<li><lable>parent Page</lable><input type="text" name="page_parent_name" value="'.$this->pageVar['page_parent_name'].'"></li>
							<li><lable>page Template</lable><input type="text" name="page_template" value="'.$this->pageVar['page_template'].'"></li>
							<li><lable>keywords</lable><textarea name="page_keywords" class="shorter">'.$this->pageVar['page_keywords'].'</textarea></li>
							<li><lable>Description</lable><textarea name="page_description" class="shorter">'.$this->pageVar['page_description'].'</textarea></li>
							<li><lable>Extra Style sheets</lable><textarea name="page_extra_stylesheets" class="shorter">'.$this->pageVar['page_extra_stylesheets'].'</textarea></li>
							<li><lable>Javascript</lable><textarea name="page_javascript" class="shorter">'.$this->pageVar['page_javascript'].'</textarea></li>
							<li><lable>Page Content</lable><textarea name="page_content" class="longer">'.$this->pageVar['page_content'].'</textarea></li>
							<li><lable>Page Sub Content</lable><textarea name="page_subcontent" class="longer">'.$this->pageVar['page_subcontent'].'</textarea></li>
							<li>
								<input type="hidden" name="add_submitted" value="1">
								<input type="submit" value="Submit Changes" class="submit">
							</li>
						</ul>
					</section>
					</form>
				</div>
				<!-- end of page content -->
		';
		return $contentArea;
	}	
	
	public function footer()
	{
		$footer = '
				<!-- start of footer -->
				<div id="footer">
				</div>
				<!-- end of footer -->
		';
		return $footer;
	}
	
	public function endBodyWrap()
	{
		$endBodyWrap = '
				</div>
			<!-- end wrapper -->
		</body>
		</html>
		';
		return $endBodyWrap;
	}
}

?>