<?

class adminEditPage extends adminBaseFramework
{
		
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
	
	public function contentArea()
	{
		$contentArea = '
				<!-- start of page content -->
				<div id="page_content">
					<form action="/editor/index.php" method="post"> 
					<section id="content">
						<ul>
							<li><lable>title</lable><input type="text" name="page_title" value="'.$this->pageVar['page_title'].'"></li>
							<li><lable>page Name</lable><input type="text" name="page_name" value="'.$this->pageVar['page_name'].'"></li>
							<li><lable>parent Page</lable><input type="text" name="page_parent_name" value="'.$this->pageVar['page_parent_name'].'"></li>
							<li><lable>page Template</lable><input type="text" name="page_template" value="'.$this->pageVar['page_template'].'"></li>
							<li><lable>keywords</lable><textarea name="page_keywords" class="shorter">'.$this->pageVar['page_keywords'].'</textarea></li>
							<li><lable>Description</lable><textarea name="page_description" class="shorter">'.$this->pageVar['page_description'].'</textarea></li>
							<li><lable>Extra Style sheets</lable><textarea name="page_extra_stylesheets" class="shorter">'.$this->pageVar['page_extra_stylesheets'].'</textarea></li>
							<li><lable>Javascript</lable><textarea name="page_javascript" class="shorter">'.$this->pageVar['page_javascript'].'</textarea></li>
							<li><lable>Page Content</lable><textarea name="page_content" class="longer">'.$this->pageVar['page_content'].'</textarea></li>
							<li><lable>Page Sub Content</lable><textarea name="page_subcontent" class="longer">'.$this->pageVar['page_subcontent'].'</textarea></li>
							<li>
								<input type="hidden" name="edit_submitted" value="1">
								<input type="hidden" name="edit_id" value="'.$this->pageVar['page_id'].'">
								<input type="submit" value="Submit Changes" class="submit">
							</li>
						</ul>
					</section>
					</form>
				</div>
				<!-- end of page content -->
		';
		return $contentArea;
	}
}
?>