<?

class adminPage extends adminBaseFramework
{
		
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
}
?>