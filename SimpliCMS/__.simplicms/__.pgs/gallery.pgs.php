<?
class galleryPage extends baseFramework
{
	private $galleries = array('<!-- photo_frames gallery -->','<!-- art gallery -->','<!-- gifts gallery -->');
	private $galleryVars;
		
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->navigationArea();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
	
	public function contentArea()
	{
		// SET GALLERY IN CONTENT
		$this->pageVar['page_content'] = str_replace($this->findGalleryArea(), $this->buildGallery(), $this->pageVar['page_content']);
				
		// CONTENT CREATION
		$contentArea = '
				<!-- start of page content -->
				<div id="content" class="clearfix">
					
		            <section id="main_content">
		            	'.$this->pageVar['page_content'].'
		            </section>
		            <aside id="sub_content">
		            	'.$this->pageVar['page_subcontent'].'
		            </aside>
				</div>
				<!-- end of page content -->
		';
		return $contentArea;
	}
	
	private function findGalleryArea()
	{
		$galleryArea;
		for ($i = 0; $i < count($this->galleries); $i++) 
		{
			$current = strstr($this->pageVar['page_content'], $this->galleries[$i]);
			if($current != '') {$galleryArea = $current;}
		}
		return $galleryArea;
	}
	
	private function setCurrentGallery()
	{
		//FIND GALLERY STRING
		$galleryArea = $this->findGalleryArea();		
		$remove = array('<!--','gallery','-->',' ');
		return str_replace($remove, '', $galleryArea);
	}
	
	private function buildGallery()
	{
		// GET THE GALLERY NAME
		$galleryNeeded = $this->setCurrentGallery();
		// GET GALLERY INFORMATION FROM DATABASE
		$this->galleryVars = $this->database->getGalleryInformation($galleryNeeded);
		// BUILD SEQUENCE OF IMAGES
		$gallery = '<section id="image_gallery">';
		$gallery.= '	<div id="scrollable"><div id="items">'.$this->getThumbnails().'</div></div>
						<div id="scrollers" class="clearfix"><div id="previous" class="prev browse left">previous</div><div id="next" class="next browse right">next</div></div>
						<div id="group_numbers" class="clearfix">'.$this->getListNumber().'</div>
		';
		
		return $gallery.= '</section>';
	}
	
	private function getThumbnails()
	{
		$thumbList;
		$x=0;
		for($i=0;$i<count($this->galleryVars);$i++)
		{
			if($x == 0){ $thumbList.= '<div>'; }
			$thumbList.= '<a href="/assets/img/galleries/'.$this->galleryVars[$i][2].'" rel="gallery" class="gallery_link" title="'.$this->galleryVars[$i][4].'"><img src="/assets/img/galleries/'.$this->galleryVars[$i][3].'" width="100" height="74" alt="'.$this->galleryVars[$i][4].'"></a>';				
			if($x == 4) { $thumbList.= '</div>'; $x=0; } else { $x++; }
		}
		if($x>0 && $x!=4) {$thumbList.= '</div>';}
		return $thumbList;
	}
	
	private function getListNumber()
	{
		$numberList;
		$x=0;
		$numberList.= '<ul class="gallery_group clearfix">';
		for($i=0; $i<count($this->galleryVars); $i++)
		{
			if($number<count($this->galleryVars)) $number = $i + 1;
			if($x == 0 && $i == 0){ $numberList.= '<li class="group clearfix active">'; }
			else if ($x == 0){ $numberList.= '<li class="group clearfix">'; }			
			$numberList.= '<span>'.$number.'</span>';				
			if($x == 4) { $numberList.= '</li>'; $x=0; } else { $x++; }	
		}
		if($x>0 && $x!=4) {$numberList.= '</li>';}
		$numberList.= '</ul>';
		return $numberList;
	}
}
?>