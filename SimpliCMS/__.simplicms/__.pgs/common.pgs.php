<?

class commonPage extends baseFramework
{
		
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->navigationArea();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
}
?>