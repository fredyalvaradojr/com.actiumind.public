<?

class adminEditsComplete extends adminBaseFramework
{
		
	public function buildPage()
	{
		echo $this->htmlHead();
		echo $this->startBodyWrap();
		echo $this->header();
		echo $this->contentArea();
		echo $this->footer();
		echo $this->endBodyWrap();
	}
	
	public function contentArea()
	{
		$contentArea = '
				<!-- start of page content -->
				<div id="page_content">
					<section id="content">
						<p>Your updates/addition/edits have been made.</p>
					</section>
				</div>
				<!-- end of page content -->
		';
		return $contentArea;
	}
}
?>