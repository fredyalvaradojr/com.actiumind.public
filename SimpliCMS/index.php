<?php
//session_start(); // START SESSION IF NEEDED

// ERROR REPORTING
//error_reporting(E_ALL);
error_reporting(0);

// WEBROOT AND INITIAL CONFIGURATION FILE
define('SITE_ROOT', str_replace('index.php', '', __FILE__));
echo SITE_ROOT;
$local_server = 'server';
include '__.simplicms/__.incld/global.config.inc.php';
/*------
 sets the variables for the browsed page
 check to see what page is needed and gets that pages cotent from the datbase
 by setting the page array variable $dbPageVars	 
*/

$page = $_GET['page'];
$subPage = $_GET['sub_page'];

if(isset($subPage))
{
	$dbPageVars = $database->getPage(globalFunctions::cleanURLName($subPage), 'pages');
}
else if(isset($page))
{
	$dbPageVars = $database->getPage(globalFunctions::cleanURLName($page), 'pages');
}
else
{
	$page = 'home';
	$dbPageVars = $database->getPage(globalFunctions::cleanURLName($page), 'pages');
}
// END OF DATABASE CHECK

// FORM PROCESSOR
checkForForm();

// MAILCHIMP API KEY
$dbPageVars['page_subcontent'] = signupNewsletter('194dfcce0ff491d0630a9e3f5f479453-us2', 'c3adeae543', $dbPageVars['page_subcontent']);

// START REDIRECT CHECK
if (!isset($subPage))
{
	$check = $dbPageVars['page_parent_name'].'/';
	if($check !== '/')
	{	
		globalFunctions::permanentRedirect($page);
	}		
}
else
{
	globalFunctions::permanentRedirect(globalFunctions::getCurrentUrl());
}
// END REDIRECT CHECK

// create the page from the template page thats required and create a new instance of that variable
if ($dbPageVars['page_template'] == ''){ globalFunctions::permanentRedirect('404'); }
$renderPage = new $dbPageVars['page_template']();

// set dabatase object for in class query
$renderPage->database = $database;
	
// sets the current page for special needs e.g. title activation, image swap
$renderPage->__set('page_name', $dbPageVars['page_name']);

// checks to see if its a subpage of the page
if(isset($subPage))
{
	$renderPage->__set('page_subpage_name', $dbPageVars['page_parent_name']);
	$renderPage->__set('page_name', $dbPageVars['page_name']);
}

if(isset($dbPageVars['page_title']))
	$renderPage->__set('page_title',$dbPageVars['page_title']);

if(isset($dbPageVars['page_keywords']))
	$renderPage->__set('page_keywords',$dbPageVars['page_keywords']);

if(isset($dbPageVars['page_description']))
	$renderPage->__set('page_description',$dbPageVars['page_description']);

if(isset($dbPageVars['page_javascript']))
	$renderPage->__set('page_javascript',$dbPageVars['page_javascript']);

if(isset($dbPageVars['page_extra_stylesheets']))
	$renderPage->__set('page_extra_stylesheets',$dbPageVars['page_extra_stylesheets']);

if(isset($dbPageVars['page_images_content']))
	$renderPage->__set('page_images_content',$dbPageVars['page_images_content']);

$renderPage->__set('page_content', $dbPageVars['page_content']);

$renderPage->__set('page_subcontent', $dbPageVars['page_subcontent']);

$renderPage->buildPage();


function checkForForm()
{
	global $formProcess;
	// atachment
	//globalFunctions::checkUploadedResume($_FILES['career_resume']);
	//move_uploaded_file($_FILES["career_resume"]["tmp_name"], SITE_ROOT.'/uploads/resume/'.basename($_FILES['career_resume']['name']));
	//$attachment = Swift_Attachment::fromPath(SITE_ROOT.'/uploads/resume/'.basename($_FILES['career_resume']['name']));	
	if($_POST['form_submit'] == 1)
	{
		// init swift mail lib
		$swiftMail = Swift_Message::newInstance();
		// mail()
		$transport = Swift_MailTransport::newInstance();
		// create a mailer for message
		$mailer = Swift_Mailer::newInstance($transport);
		// check type of email
		switch($_POST['type'])	
		{
			case 'contact_us':
				$contactList = array('First Name:'=>$_POST['firstName'], 'Last Name:' =>$_POST['lastName'], 'Email:'=>$_POST['email'], 'Home Address:'=>$_POST['homeAddress'], 'City:'=>$_POST['city'], 'State:'=>$_POST['state'], 'Zip:'=>$_POST['zip'], 'Comments:'=>$_POST['whyApply']);
				$formProcess->processForm($swiftMail, $mailer, $contactList, 'smartplumbinginc.com contact us submission');
				break;
		}
	}
}

function signupNewsletter($apikey, $list, $content)
{
	if($_POST['signup_submit'] == 1)
	{
		$api = new MCAPI($apikey);		 
		$retval = $api->listSubscribe( $list, $_POST['email'], '','html', 'false' );		 
		if ($api->errorCode)
		{
			switch($api->errorCode)
			{
				case '214' :
					$content = str_replace('<input type="text" name="email" id="email">', '<input type="text" name="email" id="email"><label class="error">That email address has been<br>previously signed up.</label>', $content);
					break;
				case '502' :					
					$content = str_replace('<input type="text" name="email" id="email">', '<input type="text" name="email" id="email"><label class="error">There was an error with your email address.</label>', $content);
					break;
			}
		} 
		else {
		    header("Location: /signup-successful");
		    exit;
		}
	}
	return $content; 
}
?>