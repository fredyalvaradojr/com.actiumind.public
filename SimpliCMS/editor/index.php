 <?php
 	// WEBROOT AND INITIAL CONFIGURATION FILE
	define('SITE_ROOT', str_replace('editor/index.php', '', __FILE__));
	
	$local_server = 'server';
	require '../__.simplicms/__.incld/admin.config.inc.php';
	
	// set the form values if they exist
	if(isset($_GET['edit_page']))
	{
		$dbPageVars = $database->getPage(globalFunctions::cleanURLName($_GET['edit_page']), 'pages');
		// instance of object
		$homepage = new adminEditPage();
		// form values
		$homepage->__set('page_id', $dbPageVars['page_id']);
		$homepage->__set('page_title', $dbPageVars['page_title']);
		$homepage->__set('page_name', $dbPageVars['page_name']);
		$homepage->__set('page_parent_name', $dbPageVars['page_parent_name']);
		$homepage->__set('page_template', $dbPageVars['page_template']);
		$homepage->__set('page_keywords', $dbPageVars['page_keywords']);
		$homepage->__set('page_description', $dbPageVars['page_description']);
		$homepage->__set('page_extra_stylesheets', $dbPageVars['page_extra_stylesheets']);
		$homepage->__set('page_javascript', $dbPageVars['page_javascript']);
		$homepage->__set('page_content', htmlentities($dbPageVars['page_content']));
		$homepage->__set('page_subcontent', htmlentities($dbPageVars['page_subcontent']));
	}
	else if ($_POST['edit_submitted']==1)
	{
		$homepage = new adminEditsComplete();
		
		$page_id =					$_POST['edit_id'];
		$page_title = 				globalFunctions::cleanQueryInputs($_POST['page_title']);
		$page_name = 				$_POST['page_name'];
		$page_parent_name =			$_POST['page_parent_name'];
		$page_template = 			globalFunctions::cleanQueryInputs($_POST['page_template']);
		$page_keywords = 			globalFunctions::cleanQueryInputs($_POST['page_keywords']);
		$page_description = 		globalFunctions::cleanQueryInputs($_POST['page_description']);
		$page_extra_stylesheets =	globalFunctions::cleanQueryInputs($_POST['page_extra_stylesheets']);
		$page_javascript = 			globalFunctions::cleanQueryInputs($_POST['page_javascript']);
		$page_content = 			($_POST['page_content']);
		$page_subcontent = 			($_POST['page_subcontent']);			
		
		$sql = " UPDATE pages ";
		$sql = $sql . " SET page_title='$page_title', page_name='$page_name', page_parent_name='$page_parent_name', page_template='$page_template', page_keywords='$page_keywords', page_description='$page_description', page_extra_stylesheets='$page_extra_stylesheets', page_javascript='$page_javascript', page_content='$page_content', page_subcontent='$page_subcontent' ";
		$sql = $sql . " WHERE page_id = '$page_id'";
		$database->query($sql);
		
	}
	else if ($_POST['add_submitted']==1)
	{
		$homepage = new adminEditsComplete();
		
		$page_id =					$_POST['edit_id'];
		$page_title = 				globalFunctions::cleanQueryInputs($_POST['page_title']);
		$page_name = 				$_POST['page_name'];
		$page_parent_name =			$_POST['page_parent_name'];
		$page_template = 			globalFunctions::cleanQueryInputs($_POST['page_template']);
		$page_keywords = 			globalFunctions::cleanQueryInputs($_POST['page_keywords']);
		$page_description = 		globalFunctions::cleanQueryInputs($_POST['page_description']);
		$page_extra_stylesheets =	globalFunctions::cleanQueryInputs($_POST['page_extra_stylesheets']);
		$page_javascript = 			globalFunctions::cleanQueryInputs($_POST['page_javascript']);
		$page_content = 			($_POST['page_content']);
		$page_subcontent = 			($_POST['page_subcontent']);		
		
		$sql = " INSERT INTO pages ";
		$sql = $sql . " (page_title, page_name, page_parent_name, page_template, page_keywords, page_description, page_extra_stylesheets, page_javascript, page_content, page_subcontent) ";
		$sql = $sql . " VALUES ('$page_title', '$page_name', '$page_parent_name', '$page_template', '$page_keywords', '$page_description', '$page_extra_stylesheets', '$page_javascript', '$page_content', '$page_subcontent') ";
		$database->query($sql);
		
	}
	else
	{
		$homepage = new adminPage();

	}
	
	
	
	$menu = $database->getAdminMenu("SELECT page_name FROM pages ORDER BY page_name");
		
	$homepage->__set('menu', $menu);
	
	$homepage->buildPage();
	
?>