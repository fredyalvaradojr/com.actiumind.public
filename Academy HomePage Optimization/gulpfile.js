/*
partial Gulp file to show
critical CSS build and global optimized files
**-- please note: not the original/functional Gulp file
 */

var	globalOrderOptimized = [
        'JavaScript/global/generalComponents.js',
        'JavaScript/global/globalComponents.js',
        'JavaScript/global/headerComponents.js',
        'JavaScript/global/footerComponents.js',
        'JavaScript/global/focusTrap.js',
        'JavaScript/global/signin.js',
        'JavaScript/global/search.js',
        'JavaScript/global/minicart.js',
        'JavaScript/global/localStoreInventory.js',
        'JavaScript/touchSwipe.js'
    ];


gulp.task('default', ['watchSCSS', 'watchJS', 'scss', 'js', 'fonts']);

/* ====================================================================================
 Compile SCSS to VDI
 ==================================================================================== */
gulp.task('scssBuild', function () {

    gutil.log('Compiling SCSS to VDI');
    var tasks = files.map(function (file) {
        return gulp.src('./Styles/' + file)
            .pipe(plumber({ errorHandler: onError }))
            .pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'nested'}))
            //.pipe(gulp.dest( cssDest ))
            //.pipe(gulp.dest( cssDestLocal ))
            .pipe(rename({ suffix: '.min' }))
            .pipe(cleanCSS())
            //.pipe(gulp.dest( cssDestLocal ))
            .pipe(sourcemaps.write("../maps/css"))
            .pipe(gulp.dest( cssDestLocal ));
    });
    gutil.log('Compiling SCSS to VDI :: critical auto create');
    return gulp.src("/uidist/css/z.foundation.critical.min.css")
        .pipe(rename("CommonCSSToIncludeCritical.jspf"))
        .pipe(gulp.dest("/Common/"));

});

gulp.task('scss', ['scssBuild'],  function () {

    gutil.log('Compiled SCSS to VDI');
    notifier.notify({
        'title': 'SCSS Build',
        'message': 'Successful'
    });

});

/* ====================================================================================
 Compile JS files to platforms
 ==================================================================================== */
gulp.task('jsBuild', function() {
    function jsGulpSrc(list, name) {
        return gulp.src(list)
            .pipe(plumber({ errorHandler: onError }))
            .pipe(sourcemaps.init())
            .pipe(concat('z.'+name+'.min.js'))
            .pipe(babel({
                presets: ['es2015'],
                compact: "true"
            }))
            .pipe(uglify())
            .pipe(sourcemaps.write("../maps/js"))
            .pipe(gulp.dest(jsDestLocal));

    }
    function jsGulpSrcNoBabel(list, name) {
        return gulp.src(list)
            .pipe(plumber({ errorHandler: onError }))
            .pipe(concat('z.'+name+'.min.js'))
            .pipe(gulp.dest(jsDestLocal));
    }

    gutil.log('Concatenating JS to VDI');

    gutil.log('Concat Global JS');
    var globalJS = jsGulpSrc(globalOrder, "main");

    gutil.log('Concat Store Locator JavaScript');
    var storeLocatorJS = jsGulpSrc(storeLocator, "storeLocator");

    gutil.log('Concat My Account JavaScript');
    var myAccountJS = jsGulpSrc(myAccount, "myAccount");

    gutil.log('Concat Vendor JavaScript');
    var vendorJS = jsGulpSrc(vendors, "vendors");

    gutil.log('Concat global-opt Optimized JavaScript');
    var globalOrderOptimizedJS = jsGulpSrc(globalOrderOptimized, "global-opt");

    gutil.log("Concat PDP vendors list");
    var vendorsPDPJS = jsGulpSrcNoBabel(vendorsPDP, "vendors-pdp");

    return merge(globalJS, storeLocatorJS, myAccountJS, vendorJS, globalOrderOptimizedJS, vendorsPDPJS);
});

gulp.task('js', ['jsBuild'], function() {

    gutil.log('Concatenated JS to VDI');
    notifier.notify({
        'title': 'JS Build',
        'message': 'Successful'
    });

});

/* ====================================================================================
 Add Fonts to build folder
 ==================================================================================== */
gulp.task('fonts', function() {
    gulp.src('./Fonts/*.{ttf,woff,eof,svg}')
        .pipe(gulp.dest(fontsDestLocal));
});

/* ====================================================================================
 Watch
 ==================================================================================== */
gulp.task('watchSCSS', function () {

    watcher = gulp.watch(['./Styles/**/*.scss'], ['scss']);
    watcher.on('change', function (event) {
        gutil.log('Changed file ' + event.path + '. Compiling and deploying SCSS...');
        notifier.notify({
            'title': 'SCSS File Changed',
            'message': 'Compiling and deploying SCSS...'
        });
    });
});

gulp.task('watchJS', function () {

    watcher = gulp.watch(['./JavaScript/**/*.js'], ['js']);
    watcher.on('change', function (event) {
        gutil.log('Changed file ' + event.path + '. Compiling and deploying JS...');
        notifier.notify({
            'title': 'JS File Changed',
            'message': 'Compiling and deploying JS...'
        });
    });
});

/* ====================================================================================
 error function for plumber
 ==================================================================================== */

var onError = function (err) {

    console.log(err);
    notifier.notify({
        'title': 'Build Error',
        'message': err
    });
    this.emit('end');

};
