// Search rewrite functionality

class search {

    // Class attributes
    constructor() {
        this.cachedSuggestionsBrand = {};
        this.cachedSuggestionsCategory = {};
        this.cacheSuggestionsLimit = 4;
        this.searchDelayTimer = "";
        this.requestSubmitted = false;
        this.searchHistoryCookieName = "searchHistory";
        this.searchInputNode = jQuery("#rh-search-text"); // searchInputNode == RHSearchTextInput
        this.cachedSuggestionsNode = jQuery("#rh-search-autosuggestion-right");
        this.suggestionsNode = jQuery("#rh-search-autosuggestion");
        this.historyNode = jQuery("#rh-search-result-history");
        this.goNode = jQuery("#rh-search-submit");
        this.searchFormNode = jQuery("#rh-search-form");
        this.searchOverlay = jQuery("#rh-search-overlay");
        this.searchOverlayNode = jQuery("#rh-search-overlay");
        this.anchorsPresent = [];
        //SearchJS.staticContentHeaderHistory = storeNLS["HISTORY"]; <-- need to check use
        if(document.getElementById('CachedSuggestionsURL') != null) {
            this.cachedSuggestionsLink = `${GeneralComponentsClass.returnAbsoluteURL()}${document.getElementById('CachedSuggestionsURL').value}`;
        }
        if(document.getElementById('SearchAutoSuggestServletURL') != null) {
            this.autoSuggestLink = `${GeneralComponentsClass.returnAbsoluteURL()}${document.getElementById('SearchAutoSuggestServletURL').value}`;
        }
        // load cached list
        GeneralComponentsClass.getAjax(`${this.cachedSuggestionsLink}`, this.successCachedSuggestions, this.errorCachedSuggestions, this);
        this.searchHistoryCookieArr = this.createSearchHistoryCookieArray();
    }

    displaySearchTypeAheadResults() {
        //console.debug("displaySearchTypeAheadResults");
    }

    successCachedSuggestions(t, r) {
        var _this = t;
        t.cachedSuggestionsBrand = r["cachedSuggestions"]["Brand"];
        t.cachedSuggestionsCategory = r["cachedSuggestions"]["Category"];
    }

    errorCachedSuggestions() {
        console.debug("errorCachedSuggestions :: error");
    }

    returnAutoSuggestionTerms(e, searchTerm) {
        var searchTerm = encodeURIComponent(searchTerm.replace(/[^A-Za-z0-9_ ]/g, "").trim());
        //console.debug("searchTerm: ", searchTerm);
        var link = `${this.autoSuggestLink}&term=${searchTerm}`;

        localStorage.setItem('onsite_search_term', searchTerm);
        localStorage.setItem('onsite_search_type','reg');

        GeneralComponentsClass.getAjax(`${link}`, this.returnAutoSuggestionTermsSuccess, this.returnAutoSuggestionTermsError, this);
    }

    returnAutoSuggestionCachedTermsContent(b, c) {
        var content = "";
        if (c.length > 0) {
            content +=  `
                        <h5>Category</h5>
                        <ul>
                            ${c}
                        </ul>
                        `;
        }
        if (b.length > 0) {
            content +=  `
                        <h5>Brand</h5>
                        <ul>
                            ${b}
                        </ul>
                        `;
        }
        return content;
    }

    returnAutoSuggestionCachedTerms(e, searchTerm) {
        // get the first 4 results that match from chached results for category and brand
        var count = 0,
            brandListContent = "",
            categoryListContent = "",
            lengthCachedSuggestionsBrand = this.cachedSuggestionsBrand.length,
            lengthCachedSuggestionsCategory = this.cachedSuggestionsCategory.length,
            i,
            x;

        for (i = 0; i < lengthCachedSuggestionsBrand; i++) {
            var termFromArrayName = this.cachedSuggestionsBrand[i]["entryName"];
            var termFromArrayLink = this.cachedSuggestionsBrand[i]["entryNameUrl"];
            if(termFromArrayName.search(new RegExp(searchTerm, "ig")) !== -1 && count < this.cacheSuggestionsLimit){
                let termHighlight = termFromArrayName.replace(new RegExp(searchTerm, "ig"), function (m) {
                        return `<span class="highlight">${m}</span>`;
                    }
                );

                brandListContent += `
                                    <li>
                                        <a class="autosug-item" data-section="brand" href="${termFromArrayLink}">${termHighlight}</a>
                                    </li>
                                    `;
                count++;
            } else if (count == this.cacheSuggestionsLimit) {
                count = 0;
                break;
            }
        }

        count = 0;
        for (x = 0; x < lengthCachedSuggestionsCategory; x++) {
            var termFromArrayName = this.cachedSuggestionsCategory[x]["entryName"];
            var termFromArrayLink = this.cachedSuggestionsCategory[x]["entryNameUrl"];
            if(termFromArrayName.search(new RegExp(searchTerm, "ig")) !== -1 && count < this.cacheSuggestionsLimit){
                let termHighlight = termFromArrayName.replace(new RegExp(searchTerm, "ig"), function (m) {
                        return `<span class="highlight">${m}</span>`;
                    }
                );
                categoryListContent +=  `
                                        <li>
                                            <a class="autosug-item" data-section="category" href="${termFromArrayLink}">${termHighlight}</a>
                                        </li>
                                        `;
                count++;
            } else if (count == this.cacheSuggestionsLimit) {
                count = 0;
                break;
            }
        }

        this.cachedSuggestionsNode.html(this.returnAutoSuggestionCachedTermsContent(brandListContent, categoryListContent));
    }

    searchHistoryCookieCheck() {
        var _this = Search,
            searchText = jQuery.trim(_this.searchInputNode.val()),
            searchHistoryCookie = GeneralComponentsClass.readCookie(_this.searchHistoryCookieName);

        if (searchHistoryCookie == null) {
            // cookie does not exist, create it add the search term and send them on their way
            let tempSearchText = `|${searchText}`;
            GeneralComponentsClass.createCookie(_this.searchHistoryCookieName, tempSearchText, 10000);
        }
        else {
            // cookie exists, add the search term and send them on their way
            let tempSearchText = `${searchHistoryCookie}|${searchText}`;
            GeneralComponentsClass.createCookie(_this.searchHistoryCookieName, tempSearchText);
        }
        localStorage.setItem('onsite_search_term', searchText);
        localStorage.setItem('onsite_search_type', `ta:reg`);
        // if class contains cat or brand href = url else its a search term
        //searchText = searchText.split(' ').join('+');
        document.location.href = `${GeneralComponentsClass.returnAbsoluteURL()}browse/SearchDisplay?searchTerm=${encodeURIComponent(searchText)}`;
    }

    createSearchHistoryCookieArray() {
        var searchHistoryCookie = GeneralComponentsClass.readCookie(this.searchHistoryCookieName);
        if (searchHistoryCookie != null) {
            var list = searchHistoryCookie.split("|");
            //list.shift();
            return list;
        }
        else {
            return null;
        }
    }

    returnAutoSuggestionHistoryTermsUnique(arr) {
        var u = [];
        jQuery.each(arr, function(i, el){
            if(jQuery.inArray(el, u) === -1) u.push(el);
        });
        return u;
    }

    returnAutoSuggestionHistoryTerms(e, searchTerm) {
        // check if cookie exists
        var _this = Search,
            searchHistoryCookie = GeneralComponentsClass.readCookie(_this.searchHistoryCookieName),
            searchHistoryCookieArray = new Array();
        //console.debug("searchHistoryCookie: ", searchHistoryCookie);
        if(_this.searchHistoryCookieArr != null) {
            // check for partial term match
            var list = _this.searchHistoryCookieArr,
                length = _this.searchHistoryCookieArr.length,
                i;
            for (i = 0; i < length; i++) {
                if(list[i].match(new RegExp(searchTerm, "i") )) {
                    searchHistoryCookieArray.push(list[i]);
                }
                else {
                    //console.debug("no match");
                }
            }
        }
        else {
            //console.debug("history cookie null");
        }
        //console.debug("searchHistoryCookieArray: ", searchHistoryCookieArray);
        if(searchHistoryCookieArray.length > 0) {
            var uniqueArr = _this.returnAutoSuggestionHistoryTermsUnique(searchHistoryCookieArray),
                uniqueArrLength = uniqueArr.length < 4 ? uniqueArr.length : 4,
                historyTerms = "",
                x;

            for (x = 0; x < uniqueArrLength; x++) {
                var cleanTerm = uniqueArr[x].replace(/ /g, '+');
                let termHighlight = uniqueArr[x].replace(new RegExp(searchTerm, "ig"), function (m) {
                        return `<span class="highlight">${m}</span>`;
                    }
                );
                historyTerms += `<li><a class="autosug-item" data-type="autosug" data-section="history" href="${GeneralComponentsClass.returnAbsoluteURL()}browse/SearchDisplay?searchTerm=${encodeURIComponent(cleanTerm)}">${termHighlight}</a></li>`;
            }

            historyTerms = `
                            <h5>History</h5>
                            <ul>
                            ${historyTerms}
                            </ul>
                            `;

            _this.historyNode.html(historyTerms);
        }
        else {
            _this.historyNode.html("");
        }
    }

    autoSuggestClickEnterInteraction(e) {
        e.preventDefault();
        var _this = Search,
            searchText = jQuery(this).text(),
            sugType = jQuery(this).data('type'),
            sugSection = jQuery(this).data('section');
        // check if history cookie exists
        var searchHistoryCookie = GeneralComponentsClass.readCookie(_this.searchHistoryCookieName);
        if (searchHistoryCookie == null) {
            // cookie does not exist, create it add the search term and send them on their way
            let tempSearchText = `|${searchText}`;
            GeneralComponentsClass.createCookie(_this.searchHistoryCookieName, tempSearchText, 10000);
        }
        else {
            // cookie exists, add the search term and send them on their way
            let tempSearchText = `${searchHistoryCookie}|${searchText}`;
            GeneralComponentsClass.createCookie(_this.searchHistoryCookieName, tempSearchText);
        }
        localStorage.setItem('onsite_search_term', searchText);
        if (sugSection == "history") {
            localStorage.setItem('onsite_search_type', 'ta:history');
        }
        else {
            localStorage.setItem('onsite_search_type', `ta:${sugSection}`);
        }
        // if class contains cat or brand href = url else its a search term
        if (sugType == "autosug") {
            //searchText = searchText.split(' ').join('+');
            document.location.href = `${GeneralComponentsClass.returnAbsoluteURL()}browse/SearchDisplay?searchTerm=${encodeURIComponent(searchText)}`;
        }
        else {
            document.location.href = jQuery(this).attr("href");
        }
    }

    returnAutoSuggestionTermsSuccess(t, r) {
        var _this = t;
        _this.returnAutoSuggestionTermsBuild(r);
    }

    returnAutoSuggestionTermsError() {
        console.debug("returnAutoSuggestionTermsError :: error");
    }

    returnAutoSuggestionTermsBuild(r) {
        var container = document.getElementById("rh-search-result-dynamic"),
            typeAheadResults = r[`typeAheadResults`],
            terms = "",
            length = typeAheadResults.length,
            i;
        for (i = 0; i < length; i++) {
            var cleanTerm = jQuery(`<div>${typeAheadResults[i]}</div>`).text().replace(/ /g, '+');
            terms += `<li><a class="autosug-item" data-type="autosug" data-section="dym" href="${GeneralComponentsClass.returnAbsoluteURL()}browse/SearchDisplay?searchTerm=${cleanTerm}">${typeAheadResults[i]}</a></li>`;
        }
        container.innerHTML = terms;
        this.displaySearchTypeAheadResults();
        Search.anchorsPresent = [];
        this.suggestionsNode.find("a").each(function(i) {
            //console.debug("rebuild list of 'a' in autosug list");
            jQuery(this).data("index", `${i}`);
            Search.anchorsPresent.push(jQuery(this));
        });
    }

    searchInputNodeOnFocus(e) {
        var _this = Search;
        _this.searchOverlay.removeClass("hide");
        var nodeValue = jQuery.trim(_this.searchInputNode.val());
        if (nodeValue.length > 1 && _this.suggestionsNode.hasClass("hide")) {
            _this.searchInputNode.addClass("active");
            _this.returnAutoSuggestionTerms(e, nodeValue);
            _this.returnAutoSuggestionCachedTerms(e, nodeValue);
            _this.returnAutoSuggestionHistoryTerms(e, nodeValue);
            _this.toggleAutoSuggestionView("show");
        } else {
            _this.searchInputNode.removeClass("active");
            //clearTimeout(_this.delayTimer);
        }
    }

    searchInputNodeOnBlur(e) {
        var _this = Search;
        // check for windows
        _this.toggleAutoSuggestionView("hide-all");
    }

    autoSuggestTabInteraction(e) {
        var _this = Search,
            k = (e.keyCode ? e.keyCode : e.which);
        if (k == 9) { // tab
            //console.debug("autoSuggestTabInteraction: ", e.which, jQuery(':focus').attr('class'));
            if(jQuery(':focus').hasClass('autosug-item')) {
                _this.goNode.focus();
                e.preventDefault();
            }
        }
    }

    autoSuggestArrowInteraction(e) {
        var _this = Search,
            nodeValue = jQuery.trim(_this.searchInputNode.val()),
            k = (e.keyCode ? e.keyCode : e.which);
        //console.debug("key: ", k, nodeValue.length);
        if (k == 13 && nodeValue.length == 0) {
            e.preventDefault();
        } else if (k == 13) {
            let focusedNode = jQuery(':focus');
            if (focusedNode.attr("id") == "rh-search-text") {
                e.preventDefault();
                _this.searchInputGoInteraction(e);
            }
        }
        if (k == 27) {
            var focusedNode = jQuery(':focus');
            //console.debug("focusedNode: ", focusedNode.attr("class"));
            if (focusedNode.hasClass("autosug-item")) {
                _this.searchInputNode.focus().val("");
                _this.suggestionsNode.addClass("hide");
                _this.goNode.addClass("hide");
                var container = document.getElementById("rh-search-result-dynamic"),
                    catBrand = document.getElementById("rh-search-autosuggestion-right"),
                    history = document.getElementById("rh-search-result-history");
                history.innerHTML = container.innerHTML = catBrand.innerHTML = "";
            }

            // if focus is on arrow element then delete info in input and put focus on search input
        }
        if (k == 40) { // down arrow
            if(document.activeElement.id == "rh-search-text") {
                jQuery( _this.anchorsPresent[0] ).focus();
                _this.searchInputNode.val( _this.anchorsPresent[0].text() );
            }
            else {
                var focusedNode = jQuery(':focus'),
                    newIndex = parseInt(focusedNode.data("index"))+1;
                if (newIndex < _this.anchorsPresent.length) {
                    jQuery( _this.anchorsPresent[newIndex] ).focus();
                    _this.searchInputNode.val(_this.anchorsPresent[newIndex].text());
                } else {
                    _this.anchorsPresent[0].focus();
                    _this.searchInputNode.val(_this.anchorsPresent[0].text());
                }
            }
            e.preventDefault();
        }

        if(k == 38) { // up arrow
            // send focus to last autosuggest item if not on a list item
            if(document.activeElement.id == "rh-search-text") {
                jQuery('#rh-search-autosuggestion-results-wrap a:last')[0].focus();
            }
            else {
                var focusedNode = jQuery(':focus'),
                    newIndex = parseInt(focusedNode.data("index"))-1;
                if (newIndex < 0) {
                    _this.anchorsPresent[_this.anchorsPresent.length-1].focus();
                    _this.searchInputNode.val(_this.anchorsPresent[_this.anchorsPresent.length-1].text());
                } else {
                    _this.anchorsPresent[newIndex].focus();
                    _this.searchInputNode.val(_this.anchorsPresent[newIndex].text());
                }
            }
            e.preventDefault();
        }
    }

    searchInputNodeOnKeyInputContinue(e) {
        var _this = Search,
            nodeValue = jQuery.trim(_this.searchInputNode.val()),
            k = (e.keyCode ? e.keyCode : e.which);
        //console.debug("nodeValue searchterm: ", nodeValue);
        if (k == 40 || k == 38) {
            _this.autoSuggestArrowInteraction(e);
        }
        else {
            if (nodeValue.length > 1) {
                _this.returnAutoSuggestionTerms(e, nodeValue);
                if (jQuery(window).width() >= GeneralComponentsClass.screenSmMin) {
                    _this.returnAutoSuggestionCachedTerms(e, nodeValue);
                    _this.returnAutoSuggestionHistoryTerms(e, nodeValue);
                }
                _this.toggleAutoSuggestionView("show");
            }
            else {
                // turn off dropdown for results
                _this.searchInputNode.removeClass("active");
                _this.toggleAutoSuggestionView("hide-now");
                var container = document.getElementById("rh-search-result-dynamic"),
                    catBrand = document.getElementById("rh-search-autosuggestion-right"),
                    history = document.getElementById("rh-search-result-history");
                history.innerHTML = container.innerHTML = catBrand.innerHTML = "";
            }
            if (nodeValue.length > 0) {
                this.goNode.removeClass("hide");
                _this.searchInputNode.addClass("active");
            }
            else {
                _this.searchInputNode.removeClass("active");
                this.goNode.addClass("hide");
            }
        }
    }

    searchInputNodeOnInput(e) {
        //console.debug("searchInputNodeOnInput: ", e);
        var _this = Search,
            regex = /[\/\\^!@#%$*+?:=,<>;()|[\]{}]/g,
            key = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(key)) {
            //console.debug("invalid character");
            event.preventDefault();
            return false;
        } else {
            _this.searchInputNodeOnKeyInputContinue(e);
        }
    }

    toggleAutoSuggestionView(state) {
        var _this = Search,
            nodeValue = jQuery.trim(_this.searchInputNode.val());
        // have to check focus is not on popup, delay by 250ms
        if(_this.delayTimer){
            _this.delayTimer = null;
        }

        if (state != "hide-now") {

            _this.delayTimer = setTimeout(function () {
                let focusedNodeIdClass = jQuery(':focus').attr("id") == undefined ? jQuery(':focus').attr("class") : jQuery(':focus').attr("id");
                if (state != "hide" && (focusedNodeIdClass == "rh-search-submit" || focusedNodeIdClass == "rh-search-text" || focusedNodeIdClass == "autosug-item") ) {
                    clearTimeout(_this.delayTimer);
                    _this.delayTimer = null;
                    if(nodeValue.length > 1)
                        _this.suggestionsNode.removeClass("hide");
                    _this.searchOverlay.removeClass("hide");
                }
                else {
                    clearTimeout(_this.delayTimer);
                    _this.delayTimer = null;
                    switch (state) {
                        case "hide":
                            //console.debug("hide");
                            _this.suggestionsNode.addClass("hide");
                            break;
                        case "hide-all":
                            //console.debug("hide-all");
                            _this.suggestionsNode.addClass("hide");
                            _this.searchOverlay.addClass("hide");
                            break;
                    }
                }
            }, 250);

        }
        else {

            _this.suggestionsNode.addClass("hide");
        }
    }

    searchInputSearchInteractionCheckTerms(t) {
        if(t.length == 0){
            return false;
        }
        if (t.trim() === "*"){
            return false;
        }
        return true;
    }

    searchInputGoInteraction(e) {
        var _this = Search,
            searchTerm = _this.searchInputNode.val().trim().replace(/'|"/g, "");
        if ( _this.searchInputSearchInteractionCheckTerms(searchTerm) ) {
            _this.searchHistoryCookieCheck();
            //_this.searchFormNode[0].submit();
        }
    }

    searchOverlayInteraction(e) {
        var _this = Search;
        _this.suggestionsNode.addClass("hide");
        _this.searchOverlay.addClass("hide");
    }

    checkSearchRedirect() {
        //does search-redirect exist?
        if(document.getElementById("search-redirect") != null) {
            var searchRedirectNode = document.getElementById("search-redirect"),
                updatedSearchTerm = searchRedirectNode.getAttribute('data-term'),
                redirectURL = searchRedirectNode.getAttribute('data-displayurl');

            var searchHistoryCookie = GeneralComponentsClass.readCookie(this.searchHistoryCookieName);
            if (searchHistoryCookie == null) {
                // cookie does not exist, create it add the search term and send them on their way
                let tempSearchText = `|${updatedSearchTerm}`;
                GeneralComponentsClass.createCookie(this.searchHistoryCookieName, tempSearchText, 10000);
            }
            else {
                // cookie exists, add the search term and send them on their way
                let tempSearchText = `${searchHistoryCookie}|${updatedSearchTerm}`;
                GeneralComponentsClass.createCookie(this.searchHistoryCookieName, tempSearchText);
            }
            document.location.href = redirectURL;
        }
    }

    setEventListeners() {
        this.searchInputNode.on("focus", this.searchInputNodeOnFocus);
        this.searchInputNode.on("blur", this.searchInputNodeOnBlur);
        this.searchInputNode.on("input", this.searchInputNodeOnInput);
        this.searchInputNode.on("keydown", this.autoSuggestArrowInteraction);
        this.goNode.on("focus", this.searchInputNodeOnFocus);
        this.goNode.on("blur", this.searchInputNodeOnBlur);
        this.goNode.on("click", this.autoSuggestArrowInteraction);
        this.searchOverlayNode.on("click", this.searchOverlayInteraction);
        this.suggestionsNode.on("keydown", ".autosug-item", this.autoSuggestArrowInteraction);
        this.suggestionsNode.on("click", ".autosug-item", this.autoSuggestClickEnterInteraction);
        jQuery(window).on("keydown", this.autoSuggestTabInteraction);
    }
}

var Search = new search();
Search.checkSearchRedirect();
Search.setEventListeners();