//==========================

var $mf = 0;

var $numcheck = 0;

var $earray, $erandom = [];

$("#male").click(function(){
	$mf = 0;
	//alert($mf);
	$("#builderdiv").removeClass("hidden");
	$(".maleonly").removeClass("hidden");
	$(".genderoption").addClass("hidden");
	$("#startover").removeClass("hidden");
	$(".femaleonly").addClass("hidden");
	$("#mhair1").removeClass("hidden");
	$("#mhair7").addClass("hidden");
	$.post(
		ajaxurl,
		{
			action: 'session_data_post',
			gender: 'male'
		}
	);	
});

$("#female").click(function(){
	$mf = 1;
	//alert($mf);
	$("#builderdiv").removeClass("hidden");
	$(".genderoption").addClass("hidden");
	$(".maleonly").addClass("hidden");
	$("#startover").removeClass("hidden");
	$(".femaleonly").removeClass("hidden");
	$("#mhair7").removeClass("hidden");
	$("#mhair1").addClass("hidden");
	$.post(
		ajaxurl,
		{
			action: 'session_data_post',
			gender: 'female'
		}
	);
});

$("#startnew").click(function() {
    location.reload();
});

$("#sweaterpic").click(function() {
	$(".avatar1").css({ border: "0px" });
	//setTimeout(function(){avatarCap();},0);
	/*function avatarCap(){
		html2canvas($(".avatar1"), {
	        onrendered: function(canvas) {
	        	theCanvas = canvas;
	        	//document.body.appendChild(canvas);
	        	//Canvas2Image.saveAsPNG(canvas);
	        	//$("#img-out").append(canvas);
	            // canvas is the final rendered <canvas> element
	            //var myImage = canvas.toDataURL("image/png");
	            //window.open(myImage);
	            //$("#imgtest").attr("src", myImage);
	        },
	        allowTaint: true,
	        useCORS: true,
	        profile: true,
	    });
	}*/
    $(".avatar1").css({ border: "1px solid #000" });
    takescreenshot(function(mydata){
    	//alert(mydata);
		//$('#imgtest').attr("src", mydata);
	});
    /*$.post(
    	'/wp-admin/admin-ajax.php',
    	{
    		action: 'create_screenshot_post',
    		success: function (data){
    			alert(data);
    		}
    	}
	);*/
});

function takescreenshot(callback) {
	$.post(
    	ajaxurl,
    	{
    		action: 'create_screenshot_post'
    	}
	).done(function (data) {
		//alert(data);
		//var a = (data).toString();
		//var b = a.substring(0, a.length - 1);
		//alert(a);
		//alert(b);
		$('#imgtest').attr("src", data);
	});
};

function updateimage(response){
	alert(response);
}

$("#flat").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".shirtline").css({ fill: color.toHexString()});
    } 
});

$("#flatnewtest").minicolors({
	defaultValue: '#cccccc', 
	control: 'brightness',
	inline: 'true',
	change: function(value, opacity) {
	        $(".shirtline").css({ fill: value});
	    }
});

$("#flatnewtest2").minicolors({
	defaultValue: '#cccccc', 
	control: 'wheel',
	inline: 'true',
	change: function(value, opacity) {
	        $(".shirtline").css({ fill: value});
	    }
});



$("#flat2").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".haircolor").css({ fill: color.toHexString()});
    } 
});

$("#facecolorpicker1").click(function() {
	$(".faceline").css({ fill: "#6f5a49"});
});

$("#facecolorpicker2").click(function() {
	$(".faceline").css({ fill: "#90624a"});
});

$("#facecolorpicker3").click(function() {
	$(".faceline").css({ fill: "#ae805e"});
});

$("#facecolorpicker4").click(function() {
	$(".faceline").css({ fill: "#837052"});
});

$("#facecolorpicker5").click(function() {
	$(".faceline").css({ fill: "#b99a61"});
});

$("#facecolorpicker6").click(function() {
	$(".faceline").css({ fill: "#decf98"});
});

$("#pattern1pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".pattern1fill").css({ fill: color.toHexString()});
    } 
});

$("#pattern2pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".pattern2fill").css({ fill: color.toHexString()});
    } 
});

$("#pattern3pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".pattern3fill").css({ fill: color.toHexString()});
    } 
});

$("#pattern4pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".pattern4fill").css({ fill: color.toHexString()});
    } 
});

$("#pattern5pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".pattern5fill").css({ fill: color.toHexString()});
    } 
});

$("#pattern6pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".pattern6fill").css({ fill: color.toHexString()});
    } 
});

$("#sweater2basefill").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".sweaterbase2").css({ fill: color.toHexString()});
    } 
});

$("#sweater3basefill").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".sweaterinner3").css({ fill: color.toHexString()});
    } 
});

$("#sweater-shirt-pal").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".sweaterinner2").css({ fill: color.toHexString()});
    } 
});

$("#sweater-shirt-pal2").spectrum({
    flat: true,
    showInput: true,
    move: function(color) {
    	$(".sweaterbase3").css({ fill: color.toHexString()});
    } 
});

$("#elementsbtn").click(function(){
	$(".elements-selector").toggle();
});

$("#btnsweaterstyle1").click(function(){
	$("#sweaterstyle1").show();
	$("#sweaterstyle2").hide();
	$("#sweaterstyle3").hide();
	$(".inneroption-sweater2").hide();
	$(".inneroption-sweater3").hide();
	$(".sweater1color").show();
	$(".sweater2color").hide();
	$(".sweater3color").hide();
});

$("#btnsweaterstyle2").click(function(){
	$("#sweaterstyle1").hide();
	$("#sweaterstyle2").show();
	$("#sweaterstyle3").hide();
	$(".inneroption-sweater2").show();
	$(".inneroption-sweater3").hide();
	$(".sweater1color").hide();
	$(".sweater2color").show();
	$(".sweater3color").hide();
});

$("#btnsweaterstyle3").click(function(){
	$("#sweaterstyle1").hide();
	$("#sweaterstyle2").hide();
	$("#sweaterstyle3").show();
	$(".inneroption-sweater2").hide();
	$(".inneroption-sweater3").show();
	$(".sweater1color").hide();
	$(".sweater2color").hide();
	$(".sweater3color").show();
});

$("#btnpat1").click(function(){
	$(".pattern1pal").show();
	$(".pattern1").show();
	$(".pattern2pal").hide();
	$(".pattern2").hide();
	$(".pattern3pal").hide();
	$(".pattern3").hide();
	$(".pattern4pal").hide();
	$(".pattern4").hide();
	$(".pattern5pal").hide();
	$(".pattern5").hide();
	$(".pattern6pal").hide();
	$(".pattern6").hide();
});

$("#btnpat2").click(function(){
	$(".pattern1pal").hide();
	$(".pattern1").hide();
	$(".pattern2pal").show();
	$(".pattern2").show();
	$(".pattern3pal").hide();
	$(".pattern3").hide();
	$(".pattern4pal").hide();
	$(".pattern4").hide();
	$(".pattern5pal").hide();
	$(".pattern5").hide();
	$(".pattern6pal").hide();
	$(".pattern6").hide();
});

$("#btnpat3").click(function(){
	$(".pattern1pal").hide();
	$(".pattern1").hide();
	$(".pattern2pal").hide();
	$(".pattern2").hide();
	$(".pattern3pal").show();
	$(".pattern3").show();
	$(".pattern4pal").hide();
	$(".pattern4").hide();
	$(".pattern5pal").hide();
	$(".pattern5").hide();
	$(".pattern6pal").hide();
	$(".pattern6").hide();
});

$("#btnpat4").click(function(){
	$(".pattern1pal").hide();
	$(".pattern1").hide();
	$(".pattern2pal").hide();
	$(".pattern2").hide();
	$(".pattern3pal").hide();
	$(".pattern3").hide();
	$(".pattern4pal").show();
	$(".pattern4").show();
	$(".pattern5pal").hide();
	$(".pattern5").hide();
	$(".pattern6pal").hide();
	$(".pattern6").hide();
});

$("#btnpat5").click(function(){
	$(".pattern1pal").hide();
	$(".pattern1").hide();
	$(".pattern2pal").hide();
	$(".pattern2").hide();
	$(".pattern3pal").hide();
	$(".pattern3").hide();
	$(".pattern4pal").hide();
	$(".pattern4").hide();
	$(".pattern5pal").show();
	$(".pattern5").show();
	$(".pattern6pal").hide();
	$(".pattern6").hide();
});

$("#btnpat6").click(function(){
	$(".pattern1pal").hide();
	$(".pattern1").hide();
	$(".pattern2pal").hide();
	$(".pattern2").hide();
	$(".pattern3pal").hide();
	$(".pattern3").hide();
	$(".pattern4pal").hide();
	$(".pattern4").hide();
	$(".pattern5pal").hide();
	$(".pattern5").hide();
	$(".pattern6pal").show();
	$(".pattern6").show();
});

$("#avswitch").click(function(){
	$(".avatarcont").show();
	$("#avatarview").show();
	$(".sweatercont").hide();
	$("#sweaterview").hide();
});

$("#swswitch").click(function(){
	$(".avatarcont").hide();
	$("#avatarview").hide();
	$(".sweatercont").show();
	$("#sweaterview").show();
});

$("#eyes1").click(function(){
	$("#eyesimg").attr("src","img/eyes1.png");
});
$("#eyes2").click(function(){
	$("#eyesimg").attr("src","img/eyes2.png");
});
$("#eyes3").click(function(){
	$("#eyesimg").attr("src","img/eyes3.png");
});
$("#eyes4").click(function(){
	$("#eyesimg").attr("src","img/eyes4.png");
});
$("#eyes5").click(function(){
	$("#eyesimg").attr("src","img/eyes5.png");
});
$("#eyes6").click(function(){
	$("#eyesimg").attr("src","img/eyes6.png");
});

$("#nose1").click(function(){
	$("#noseimg").attr("src","img/nose1.png");
});
$("#nose2").click(function(){
	$("#noseimg").attr("src","img/nose2.png");
});
$("#nose3").click(function(){
	$("#noseimg").attr("src","img/nose3.png");
});
$("#nose4").click(function(){
	$("#noseimg").attr("src","img/nose4.png");
});
$("#nose5").click(function(){
	$("#noseimg").attr("src","img/nose5.png");
});
$("#nose6").click(function(){
	$("#noseimg").attr("src","img/nose6.png");
});

$("#mouth1").click(function(){
	$("#mouthimg").attr("src","img/mouth1.png");
});
$("#mouth2").click(function(){
	$("#mouthimg").attr("src","img/mouth2.png");
});
$("#mouth3").click(function(){
	$("#mouthimg").attr("src","img/mouth3.png");
});
$("#mouth4").click(function(){
	$("#mouthimg").attr("src","img/mouth4.png");
});
$("#mouth5").click(function(){
	$("#mouthimg").attr("src","img/mouth5.png");
});
$("#mouth6").click(function(){
	$("#mouthimg").attr("src","img/mouth6.png");
});

$("#ear1").click(function(){
	$("#earsimg").attr("src","img/ear1.png");
});
$("#ear2").click(function(){
	$("#earsimg").attr("src","img/ear2.png");
});
$("#ear3").click(function(){
	$("#earsimg").attr("src","img/ear3.png");
});
$("#ear4").click(function(){
	$("#earsimg").attr("src","img/ear4.png");
});
$("#ear5").click(function(){
	$("#earsimg").attr("src","img/ear5.png");
});
$("#ear6").click(function(){
	$("#earsimg").attr("src","img/ear6.png");
});

$("#hair1").click(function(){
	$("#mhair1").removeClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair2").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").removeClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair3").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").removeClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair4").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").removeClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair5").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").removeClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair6").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").removeClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").addClass("hidden");
});
$("#hair7").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").removeClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair8").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").removeClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair9").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").removeClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair10").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").removeClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair11").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").removeClass("hidden");
	$("#mhair12").addClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#hair12").click(function(){
	$("#mhair1").addClass("hidden");
	$("#mhair2").addClass("hidden");
	$("#mhair3").addClass("hidden");
	$("#mhair4").addClass("hidden");
	$("#mhair5").addClass("hidden");
	$("#mhair6").addClass("hidden");
	$("#mhair7").addClass("hidden");
	$("#mhair8").addClass("hidden");
	$("#mhair9").addClass("hidden");
	$("#mhair10").addClass("hidden");
	$("#mhair11").addClass("hidden");
	$("#mhair12").removeClass("hidden");
	$(".haircolorpal").removeClass("hidden");
});
$("#accs1").click(function(){
	$("#access1").removeClass("hidden");
	$("#access2").addClass("hidden");
	$("#access3").addClass("hidden");
	$("#access4").addClass("hidden");
	$("#access5").addClass("hidden");
	$("#access6").addClass("hidden");
});
$("#accs2").click(function(){
	$("#access1").addClass("hidden");
	$("#access2").removeClass("hidden");
	$("#access3").addClass("hidden");
	$("#access4").addClass("hidden");
	$("#access5").addClass("hidden");
	$("#access6").addClass("hidden");
});
$("#accs3").click(function(){
	$("#access1").addClass("hidden");
	$("#access2").addClass("hidden");
	$("#access3").removeClass("hidden");
	$("#access4").addClass("hidden");
	$("#access5").addClass("hidden");
	$("#access6").addClass("hidden");
});
$("#accs4").click(function(){
	$("#access1").addClass("hidden");
	$("#access2").addClass("hidden");
	$("#access3").addClass("hidden");
	$("#access4").removeClass("hidden");
	$("#access5").addClass("hidden");
	$("#access6").addClass("hidden");
});
$("#accs5").click(function(){
	$("#access1").addClass("hidden");
	$("#access2").addClass("hidden");
	$("#access3").addClass("hidden");
	$("#access4").addClass("hidden");
	$("#access5").removeClass("hidden");
	$("#access6").addClass("hidden");
});
$("#accs6").click(function(){
	$("#access1").addClass("hidden");
	$("#access2").addClass("hidden");
	$("#access3").addClass("hidden");
	$("#access4").addClass("hidden");
	$("#access5").addClass("hidden");
	$("#access6").removeClass("hidden");
});
$("#fshair1").click(function(){
	$("#facehair1").removeClass("hidden");
	$("#facehair2").addClass("hidden");
	$("#facehair3").addClass("hidden");
	$("#facehair4").addClass("hidden");
	$("#facehair5").addClass("hidden");
	$("#facehair6").addClass("hidden");
});
$("#fshair2").click(function(){
	$("#facehair1").addClass("hidden");
	$("#facehair2").removeClass("hidden");
	$("#facehair3").addClass("hidden");
	$("#facehair4").addClass("hidden");
	$("#facehair5").addClass("hidden");
	$("#facehair6").addClass("hidden");
});
$("#fshair3").click(function(){
	$("#facehair1").addClass("hidden");
	$("#facehair2").addClass("hidden");
	$("#facehair3").removeClass("hidden");
	$("#facehair4").addClass("hidden");
	$("#facehair5").addClass("hidden");
	$("#facehair6").addClass("hidden");
});
$("#fshair4").click(function(){
	$("#facehair1").addClass("hidden");
	$("#facehair2").addClass("hidden");
	$("#facehair3").addClass("hidden");
	$("#facehair4").removeClass("hidden");
	$("#facehair5").addClass("hidden");
	$("#facehair6").addClass("hidden");
});
$("#fshair5").click(function(){
	$("#facehair1").addClass("hidden");
	$("#facehair2").addClass("hidden");
	$("#facehair3").addClass("hidden");
	$("#facehair4").addClass("hidden");
	$("#facehair5").removeClass("hidden");
	$("#facehair6").addClass("hidden");
});
$("#fshair6").click(function(){
	$("#facehair1").addClass("hidden");
	$("#facehair2").addClass("hidden");
	$("#facehair3").addClass("hidden");
	$("#facehair4").addClass("hidden");
	$("#facehair5").addClass("hidden");
	$("#facehair6").removeClass("hidden");
});

var $avcurr = $("#startav");
$("#avnext").click(function(){
	if($("#avnext").hasClass("disabled")){
	}
	else {
		$avcurr = $avcurr.next();
		$avcurr.prev().hide();
		$avcurr.show();
		var $num = 8-$mf;
		if($avcurr.index() === $num){
			$("#avnext").addClass("disabled");
		}
		$("#avprev").removeClass("disabled");
	}
});
$("#avprev").click(function(){
	if($("#avprev").hasClass("disabled")){
	}
	else {
		$avcurr = $avcurr.prev();
		$avcurr.next().hide();
		$avcurr.show();
		if($avcurr.index() === 0) {
			$("#avprev").addClass("disabled");
		}
		$("#avnext").removeClass("disabled");
	}
});

var $swcurr = $("#startsw");
$("#swnext").click(function(){
	if($("#swnext").hasClass("disabled")){
	}
	else {
		$swcurr = $swcurr.next();
		$swcurr.prev().hide();
		$swcurr.show();
		if($swcurr.index() === 3){
			$("#swnext").addClass("disabled");
		}
		$("#swprev").removeClass("disabled");
	}
});
$("#swprev").click(function(){
	if($("#swprev").hasClass("disabled")){
	}
	else {
		$swcurr = $swcurr.prev();
		$swcurr.next().hide();
		$swcurr.show();
		if($swcurr.index() === 0) {
			$("#swprev").addClass("disabled");
		}
		$("#swnext").removeClass("disabled");
	}
});

$(".checkbox-inline").click(function(){
	$numcheck = $(':checkbox:checked').size();
	var $i=0;
	if($numcheck == 5){
		$(":checkbox").each(function(){
			if($(this).prop('checked')==false){
				$(this).attr('disabled','disabled');
				$(this).parent().parent().addClass('disabled');
			}
			else if($(this).prop('checked')==true){
				$earray[$i]=$(this).val();
				$i++;
			}
		});
		$("#randomize").removeClass("hidden");
	}
	else if($numcheck < 5){
		$(":checkbox").each(function(){
			$(this).removeAttr('disabled');
			$(this).parent().parent().removeClass('disabled');
		});
		$earray = [];
		$("#randomize").addClass("hidden");
	}
});

function random_elem(){
	var temp = $earray;
	temp = temp.sort(function(){
		return Math.round(Math.random() ) - 0.5;
	});
	return temp;
};

$("#randomize").click(function(){
	alert($earray);
	$erandom = random_elem();
	alert($erandom);
	$(".corner1").attr('src',$erandom[0]);
	$(".corner2").attr('src',$erandom[1]);
	$(".corner3").attr('src',$erandom[2]);
	$(".corner4").attr('src',$erandom[3]);
	$(".bigcenter").attr('src',$erandom[4]);
});
