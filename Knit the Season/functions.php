<?php

function nrg_enqueue() {
    //wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/custom.js', array( 'jquery') );
    //$ajaxphp = get_site_url() . '/wp-admin/admin-ajax.php';
    //wp_localize_script( 'customjs', 'ajaxurl', $ajaxphp );
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery.min.js', array( 'jquery') );
    wp_enqueue_script( 'jquery_bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery', 'jquery-ui' ) );
    wp_enqueue_script( 'touch_events', get_template_directory_uri() . '/js/jquery.mobile-events.min.js');
    //wp_enqueue_script( 'spectrum-js', get_template_directory_uri() . '/js/spectrum.js' );
    //wp_enqueue_script( 'html2canvas-js', get_template_directory_uri() . '/js/html2canvas.js' );
    //wp_enqueue_script( 'jquery.plugin.html2canvas-js', get_template_directory_uri() . '/js/jquery.plugin.html2canvas.js' );
    
    wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.js'); 
    wp_enqueue_script( 'c-js', get_template_directory_uri() . '/js/c.js');   

    if ( is_page_template( 'template-module.php' ) ){
        wp_enqueue_script( 'minicolors', get_template_directory_uri() . '/js/jquery.minicolors.js', array( 'jquery', 'jquery_bootstrap' ) );
        wp_enqueue_script( 'modulejs', get_template_directory_uri() . '/js/module.js', array( 'jquery', 'minicolors' ) );
        wp_enqueue_script( 'simple_pop_up', get_template_directory_uri() . '/js/jquery.simplePopup.js', array( 'modulejs' ) );
        $templateurl = get_template_directory_uri();
        $ajaxphp2 = get_site_url() . '/wp-admin/admin-ajax.php';
        wp_localize_script( 'modulejs', 'ajaxurl2', $ajaxphp2 );
        wp_localize_script( 'modulejs', 'templatedir', $templateurl );
    }
}

add_action('wp_footer', 'nrg_enqueue');

define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';
require_once dirname( __FILE__ ) . '/inc/Cloudinary.php';
require_once dirname( __FILE__ ) . '/inc/Uploader.php';
require_once dirname( __FILE__ ) . '/inc/Api.php';
//require_once dirname( __FILE__ ) . '/inc/src/Mandrill.php';
//$mandrill = new Mandrill('2yDQgl7VdIeIC6PXcaSwtA');
require_once dirname( __FILE__ ) . '/inc/PHPMailer/PHPMailerAutoload.php';
require_once dirname( __FILE__ ) . '/inc/PHPMailer/class.phpmailer.php';

\Cloudinary::config(array(
	"cloud_name" => "dmcm8adug",
	"api_key" => "947514913291832",
	"api_secret" => "EG8UUp5N0rqm2RVH-2RAqQoVFak"
));




// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
//load_template( $optionsfile );

/*
 * This is an example of how to add custom scripts to the options panel.
 * This one shows/hides the an option when a checkbox is clicked.
 *
 * You can delete it if you not using that option
 */
//add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

    jQuery('#example_showhidden').click(function() {
        jQuery('#section-example_text_hidden').fadeToggle(400);
    });

    if (jQuery('#example_showhidden:checked').val() !== undefined) {
        jQuery('#section-example_text_hidden').show();
    }

});
</script>

<?php
}

	ini_set('session.gc_maxlifetime', 3600);
	session_set_cookie_params(3600);
	session_start();

require_once dirname( __FILE__ ) . '/inc/metabox.php';

add_action('init', 'screenshots');
 
function screenshots() {
 
    $labels = array(
        'name' => 'Screenshots',
        'singular_name' => 'Screenshot',
        'add_new' => 'Add New Screenshot',
        'add_new_item' => 'Add New Screenshot',
        'edit_item' => 'Edit Screenshot',
        'new_item' => 'New Screenshot',
        'all_items' => 'All Screenshots',
        'view_item' => 'View Screenshot',
        'search_items' => 'Search Screenshots',
        'not_found' =>  'No Screenshots Found',
        'not_found_in_trash' => 'No Screenshots found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Screenshots',
    );
    //register post type
    register_post_type( 'screenshots', array(
        'labels' => $labels,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'has_archive' => true,
        'public' => true,
        'supports' => array( 'title' ),    
        'exclude_from_search' => true,
        'capability_type' => 'post',
        'rewrite' => array( 'slug' => 'screenshots' ),
        'hierarchical' => false,
        )
    );

    flush_rewrite_rules();
 
}

register_taxonomy("screenshot_type", array("screenshots"), array("hierarchical" => true, "label" => "Screenshot Types", "singular_label" => "Screenshot Type", "rewrite" => false));

add_action('init', 'uniqueurl');
 
function uniqueurl() {
 
    $labels = array(
        'name' => 'UniqueURL',
        'singular_name' => 'UniqueURL',
        'add_new' => 'Add New UniqueURL',
        'add_new_item' => 'Add New UniqueURL',
        'edit_item' => 'Edit UniqueURL',
        'new_item' => 'New UniqueURL',
        'all_items' => 'All UniqueURLs',
        'view_item' => 'View UniqueURL',
        'search_items' => 'Search UniqueURLs',
        'not_found' =>  'No UniqueURLs Found',
        'not_found_in_trash' => 'No UniqueURLs found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'UniqueURLs',
    );
    //register post type
    register_post_type( 'uniqueurl', array(
        'labels' => $labels,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'has_archive' => true,
        'public' => true,
        'supports' => array( 'title' ),    
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'rewrite' => array( 'slug' => 'uglysweater' ),
        'hierarchical' => false,
        )
    );

    flush_rewrite_rules();
 
}

register_taxonomy("uniqueurl_type", array("uniqueurls"), array("hierarchical" => true, "label" => "UniqueURL Types", "singular_label" => "UniqueURL Type", "rewrite" => false));

add_action('admin_enqueue_scripts', 'my_admin_script');
function my_admin_script()
{
    wp_enqueue_script('my-admin', get_bloginfo('template_url').'/my-admin.js', array('jquery'));
}

add_action('wp_ajax_session_data_post', 'session_data_post' );
add_action('wp_ajax_nopriv_session_data_post', 'session_data_post' );

function session_data_post() {
	//$_SESSION['gender'] = $_POST['gender'];
    //die();
    $screenshot_post = array(
        'post_title'    =>  generateRandomString(),
        'post_type'     =>  'screenshots',
        'post_status'   =>  'publish'
    );
    $_SESSION['post_id'] = wp_insert_post( $screenshot_post );
    update_post_meta($_SESSION['post_id'], '_buildervalues_gender', $_POST['gender']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_face', $_POST['face']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_skincolor', $_POST['skincolor']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_hair', $_POST['hair']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_haircolor', $_POST['haircolor']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_eyes', $_POST['eyes']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_eyescolor', $_POST['eyescolor']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_nose', $_POST['nose']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_mouth', $_POST['mouth']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_eyebrows', $_POST['eyebrows']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_facialhair', $_POST['facialhair']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_ears', $_POST['ears']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_acc', $_POST['acc']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_sweaterstyle', $_POST['swstyle']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_swbscolor', $_POST['swbscolor']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_swaccolor', $_POST['swaccolor']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_collar', $_POST['collar']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_collar4color', $_POST['collar4color']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_collar5color', $_POST['collar5color']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_pattern', $_POST['pattern']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_patterncolor', $_POST['patterncolor']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_corner1', $_POST['corner1']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_corner2', $_POST['corner2']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_corner3', $_POST['corner3']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_corner4', $_POST['corner4']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_corner5', $_POST['corner5']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_skincoloracc', $_POST['skincoloraccent']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_bgimg', $_POST['bgimg']);
    update_post_meta($_SESSION['post_id'], '_buildervalues_bgmsg', $_POST['bgmsg']);

    $posturl = get_post_permalink($_SESSION['post_id']);

    $options['force'] = 'true';
    $options['viewport'] = '1920x1080';
    //$options['format'] = 'jpg';

    function url2png_v6($url, $args) {

      $URL2PNG_APIKEY = "PF63F2FB36E5305";
      $URL2PNG_SECRET = "S_0642D9867791D";

      # urlencode request target
      $options['url'] = urlencode($url);

      $options += $args;

      # create the query string based on the options
      foreach($options as $key => $value) { $_parts[] = "$key=$value"; }

      # create a token from the ENTIRE query string
      $query_string = implode("&", $_parts);
      $TOKEN = md5($query_string . $URL2PNG_SECRET);

      return "https://api.url2png.com/v6/$URL2PNG_APIKEY/$TOKEN/png/?$query_string";

    }

    $temp = url2png_v6($posturl, $options);
    $snapshot = \Cloudinary\Uploader::upload($temp, array("format" => "jpg"));
    $snapshot_tmp = $snapshot["url"];
    //$snapshot_url = substr($snapshot_tmp, 0, -1);
    //print_r($snapshot);
    //echo $snapshot_tmp;
    wp_delete_post($_SESSION['post_id'], true);
    $uniqueurl_post = array(
        'post_title'    =>  generateRandomString(),
        'post_type'     =>  'uniqueurl',
        'post_status'   =>  'publish'
    );
    $_SESSION['uniquepost_id'] = wp_insert_post( $uniqueurl_post );
    setcookie('name',$_SESSION['uniquepost_id'], time()+(86400*7), "/");
    update_post_meta($_SESSION['uniquepost_id'], '_uniqueurlvalues_cookie', $_SESSION['uniquepost_id']);
    update_post_meta($_SESSION['uniquepost_id'], '_uniqueurlvalues_imgurl', $snapshot_tmp);
    $uniqueurl_url = get_post_permalink($_SESSION['uniquepost_id']);
    $pass_array = array($snapshot_tmp,$uniqueurl_url);
    //wp_delete_post($_SESSION['post_id'], true);
    foreach($pass_array as $a)
        echo $a.",";
    die();
}

add_action('wp_ajax_create_screenshot_post', 'create_screenshot_post' );
add_action('wp_ajax_nopriv_create_screenshot_post', 'create_screenshot_post' );

function create_screenshot_post() {
	$screenshot_post = array(
		'post_title'	=>	generateRandomString(),
		'post_type'		=>	'screenshots',
		'post_status'	=>	'publish'
	);
	$_SESSION['post_id'] = wp_insert_post( $screenshot_post );
	update_post_meta($_SESSION['post_id'], '_buildervalues_gender', $_SESSION['gender']);
    $posturl = get_post_permalink($_SESSION['post_id']);

	//$options['fullpage'] = 'true';
	$options['force'] = 'true';
	$options['viewport'] = '1280x1024';

	function url2png_v6($url, $args) {

	  $URL2PNG_APIKEY = "PF63F2FB36E5305";
	  $URL2PNG_SECRET = "S_0642D9867791D";

	  # urlencode request target
	  $options['url'] = urlencode($url);

	  $options += $args;

	  # create the query string based on the options
	  foreach($options as $key => $value) { $_parts[] = "$key=$value"; }

	  # create a token from the ENTIRE query string
	  $query_string = implode("&", $_parts);
	  $TOKEN = md5($query_string . $URL2PNG_SECRET);

	  return "https://api.url2png.com/v6/$URL2PNG_APIKEY/$TOKEN/png/?$query_string";

	}

	//$_SESSION['url'] = url2png_v6("cricinfo.com", $options);

	$temp = url2png_v6($posturl, $options);
	$snapshot = \Cloudinary\Uploader::upload($temp);
	$snapshot_tmp = $snapshot["url"];
	//$snapshot_url = substr($snapshot_tmp, 0, -1);
	//print_r($snapshot);
    echo $snapshot_tmp;

    //wp_delete_post($_SESSION['post_id'], true);
	//$_SESSION['cloud_url'] = $cloudarray["url"];
    die();

	
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

add_action('wp_ajax_send_module_email', 'send_module_email' );
add_action('wp_ajax_nopriv_send_module_email', 'send_module_email' );

function send_module_email() {
    $mail2 = new PHPMailer;
    $mail2->IsSMTP();
    $mail2->Host = 'smtp.mandrillapp.com';
    $mail2->Port = 587;
    $mail2->SMTPAuth = true;
    $mail2->Username = 'admin@amusedigital.com';
    $mail2->Password = '2yDQgl7VdIeIC6PXcaSwtA';
    $mail2->SMTPSecure = 'tls';
    $mail2->From = $_POST['from'];
    $mail2->FromName = 'Knit the Season';
    $mail2->AddAddress($_POST['to']);
    $mail2->IsHTML(true);
    $mail2->Subject = $_POST['subject'];
    $mail2->Body = '

                    <html xmlns="http://www.w3.org/1999/xhtml"><head>
                      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <title>Minty-Multipurpose Responsive Email Template</title>
                      <style type="text/css">
                         /* Client-specific Styles */
                         #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
                         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
                         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
                         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
                         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
                         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
                         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
                         a img {border:none;}
                         .image_fix {display:block;}
                         p {margin: 0px 0px !important;}
                         
                         table td {border-collapse: collapse;}
                         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
                         /*a {color: #e95353;text-decoration: none;text-decoration:none!important;}*/
                         /*STYLES*/
                         table[class=full] { width: 100%; clear: both; }
                         
                         /*################################################*/
                         /*IPAD STYLES*/
                         /*################################################*/
                         @media only screen and (max-width: 640px) {
                         a[href^="tel"], a[href^="sms"] {
                         text-decoration: none;
                         color: #ffffff; /* or whatever your want */
                         pointer-events: none;
                         cursor: default;
                         }
                         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                         text-decoration: default;
                         color: #ffffff !important;
                         pointer-events: auto;
                         cursor: default;
                         }
                         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
                         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
                         table[class="sthide"]{display: none!important;}
                         img[class="bigimage"]{width: 420px!important;height:219px!important;}
                         img[class="bigimage2"]{width: 420px!important;height:123px!important;}
                         img[class="bigimage3"]{width: 180px!important;height:38px!important;}
                         img[class="col2img"]{width: 420px!important;height:258px!important;}
                         img[class="image-banner"]{width: 440px!important;height:106px!important;}
                         td[class="menu"]{text-align:center !important; padding: 0 0 10px 0 !important;}
                         td[class="logo"]{padding:10px 0 5px 0!important;margin: 0 auto !important;}
                         img[class="logo"]{padding:0!important;margin: 0 auto !important;}

                         }
                         /*##############################################*/
                         /*IPHONE STYLES*/
                         /*##############################################*/
                         @media only screen and (max-width: 480px) {
                         a[href^="tel"], a[href^="sms"] {
                         text-decoration: none;
                         color: #ffffff; /* or whatever your want */
                         pointer-events: none;
                         cursor: default;
                         }
                         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                         text-decoration: default;
                         color: #ffffff !important; 
                         pointer-events: auto;
                         cursor: default;
                         }
                         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
                         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
                         table[class="sthide"]{display: none!important;}
                         img[class="bigimage"]{width: 260px!important;height:136px!important;}
                         img[class="bigimage2"]{width: 260px!important;height:76px!important;}
                         img[class="bigimage3"]{width: 120px!important;height:25px!important;}
                         img[class="col2img"]{width: 260px!important;height:160px!important;}
                         img[class="image-banner"]{width: 280px!important;height:68px!important;}
                         td[class="bottom-links"]{font-size: 12px!important;}
                         }
                      </style>

                      
                   </head>
                <body>
                <div class="block">
                   <!-- image + text -->
                   <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage">
                      <tbody>
                         <tr>
                            <td>
                               <table bgcolor="#ffffff" width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                                  <tbody>
                                     <tr>
                                        <td>
                                           <table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
                                              <tbody>
                                                 <tr>
                                                    <!-- start of image -->
                                                    <td align="center">
                                                       <a target="_blank" href="#"><img width="600" border="0" height="175" alt="" style="display:block; border:none; outline:none; text-decoration:none;" src="http://nrg-holiday.amusedigital.com/cms/wp-content/themes/nrg-holiday/img/reliant_KnitTheSeason_Email-template_V2_Header.jpg" class="bigimage2"></a>
                                                    </td>
                                                 </tr>
                                                 <!-- end of image -->
                                                 <!-- Spacing -->
                                                 <tr>
                                                    <td width="100%" height="20"></td>
                                                 </tr>
                                                 <!-- Spacing -->
                                                 <!-- title -->
                                                 <tr>
                                                    <td style="font-family: Rockwell, Georgia, Helvetica, arial; font-size: 24pt; color: #00aeef; text-align:center;line-height: 24pt;" st-title="rightimage-title">
                                                        Build your avatar. Knit your sweater.
                                                    </td>
                                                 </tr>
                                                 <!-- end of title -->
                                                 <!-- Spacing -->
                                                 <tr>
                                                    <td width="100%" height="20"></td>
                                                 </tr>
                                                 <!-- Spacing -->
                                                 <!-- content -->
                                                 <tr>
                                                    <td style="font-family: Rockwell, Georgia, Helvetica, arial; font-size: 21pt; color: #000000; text-align:center;line-height: 21pt;" st-content="rightimage-paragraph">
                                                       ' . $_POST['message'] . '
                                                    </td>
                                                 </tr>
                                                 <!-- end of content -->
                                                 <!-- Spacing -->
                                                 <tr>
                                                    <td width="100%" height="20"></td>
                                                 </tr>
                                                 <!-- Body Image -->
                                                 <tr>
                                                    <!-- start of image -->
                                                    <td align="center">
                                                       <a target="_blank" href="#"><img width="540" border="0" height="293" alt="" style="display:block; border:none; outline:none; text-decoration:none;" src="' . $_POST['imagelink'] . '" class="bigimage"></a>
                                                    </td>
                                                 </tr>
                                                 <!-- Spacing -->
                                                 <tr>
                                                    <td width="100%" height="40"></td>
                                                 </tr>
                                                 <!-- Spacing -->
                                                 <!-- content -->
                                                 <tr>
                                                    <td style="font-family: Verdana, Helvetica, arial; font-size: 18px; color: #000000; text-align:left;line-height: 18px;padding-left:30px;padding-right: 30px;" st-content="rightimage-paragraph">
                                                       Nothing says &#34;Happy Holidays&#34; quite like an outrageous sweater. Knit your own and shamelessly share your handiwork. When you do, Reliant will help brighten the holidays for everyone with a $1 donation to the Salvation Army.
                                                    </td>
                                                 </tr>
                                                 <!-- end of content -->
                                                 <!-- Spacing -->
                                                 <tr>
                                                    <td width="100%" height="40"></td>
                                                 </tr>
                                                 <!-- Spacing -->
                                                  <tr>
                                                    <!-- start of image -->
                                                    <td align="center">
                                                       <a target="_blank" href="' . $_POST['urllink'] . '"><img width="240" border="0" height="50" alt="" style="display:block; border:none; outline:none; text-decoration:none;" src="http://nrg-holiday.amusedigital.com/cms/wp-content/themes/nrg-holiday/img/Button-email.png" class="bigimage3"></a>
                                                    </td>

                                                 </tr>
                                                 <!-- end of image -->
                                                  <!-- Spacing -->
                                                 <tr>
                                                    <td width="100%" height="40"></td>
                                                 </tr>
                                                 <!-- Spacing -->
                                              </tbody>
                                           </table>
                                        </td>
                                     </tr>
                                  </tbody>
                               </table>
                            </td>
                         </tr>
                      </tbody>
                   </table>
                </div>
                </body></html>';
    $mail2->AltBody = 'Knit the Season';
    if(!$mail2->Send()) {
        echo 'Message not sent <br>';
        echo 'Mailer Error: ' . $mail2->ErrorInfo;
        exit;
        die();
    }
    echo 'success';
    die();
    //die();
    //echo 'done';
}

add_filter('show_admin_bar', '__return_false');
add_theme_support( 'menus' );

function main_menu() {

    $locations = array(
    );
    register_nav_menus( $locations );

}
add_action( 'init', 'main_menu' );

function url_shortcode() {
    return get_template_directory_uri();
}
add_shortcode('base_url','url_shortcode');
