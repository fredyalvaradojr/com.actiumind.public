<?php if (is_user_logged_in()) { ?>
    <a class="btn btn-danger btn-sm" id="rep-logout" href="/tasting-report">Rep Portal</a>
<?php } else { ?>
    <?php
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    ?>
        <a class="btn btn-danger btn-sm" id="rep-login-mbl" href="/sign-in">Rep Login</a>
    <?php
        } else { ?>
    <a class="btn btn-danger btn-sm" id="rep-login" href="#register-signin">Rep Login</a>
    <?php
        }
    ?>

<?php } ?>


<?php
    date_default_timezone_set('America/Chicago');
    $timeGreater;
    $timeGreaterText;
    $fill_out_report;
    $date = date('m/d/Y h:i:s a', time());
    
    if ( strtotime($start_date.' '.$time) < strtotime($date) ) {
        $timeGreater = true;
        $timeGreaterText = "true";
    } else {
        $timeGreater = false;
        $timeGreaterText = "false";
    }
    //check if the start time has exceeded 72hr limit
    //'.strtotime($start_date.' '.$time).' '.strtotime('+3 day', $timestamp).'
    $override_closed = get_field( "activate_form_", $event->id );
    
    if ( strtotime($date) > (strtotime($start_date.' '.$time)+strtotime('+3 day', $timestamp)) && $override_closed == false ) {
        $fill_out_report = '<div class="time-greater-stamp" data-greater="'.$timeGreaterText.'"></div><a class="fill-out-btn btn btn-danger btn-sm">Closed</a><i class="btn btn-warning btn-sm report-submitted">Report Submitted</i>';
    } else {
        if ($timeGreater) {
            $fill_out_report = '<div class="time-greater-stamp" data-greater="'.$timeGreaterText.'"></div><a class="fill-out-btn btn btn-success btn-sm" href="/rep-portal-area/tasting-report-beta/?d='.base64_encode($event->id).'&l='.base64_encode(get_post_meta($event_data->location_id, 'pec_venue_address', true)).'">Submit Report</a><i class="btn btn-warning btn-sm report-submitted">Report Submitted</i>';
        } else {
            $fill_out_report = '<div class="time-greater-stamp" data-greater="'.$timeGreaterText.'"></div><i class="btn btn-sm btn-info upcoming">Upcoming</i>';
        }
    }
    
    $html .= '
        <div class="table-view">
            <div class="table-view-tr">
                <div class="table-view-td a">
                    <h5>'.$start_date.'</h5>
                    <p>['.str_replace("to","-",$pec_time).']</p>
                </div>
                <div class="table-view-td b">
                    <h4><b>'.$event->title.'</b></h4>
                    <p>'.get_post_meta($event_data->location_id, 'pec_venue_address', true).'</p>
                    <p>'.get_post_meta($event_data->location_id, 'pec_venue_phone', true).'</p>
                    <p><a target="_blank" href="https://maps.google.com/maps?daddr='.get_post_meta($event_data->location_id, 'pec_venue_address', true).'" class="get-directions btn btn-sm btn-warning">Click for Directions</a></p>
                </div>
                <div class="table-view-td c">
                    <span class="event-products">'.$product_list.'</span>
                </div>
                <div class="table-view-td d">
                    '.$fill_out_report.'
                </div>
            </div>
        </div>
    ';
?>

    <div>
		<?php
    	if ( is_front_page() ) {
    	?>	
		<?php $home_post_obj = get_post( 214 ); ?>
    	<div id="page-content">
    		<section id="home" class="home-sections">
	    		<div class="section-content">					
					<h1 class="page-top-title"><?php echo the_field('page_title', 214); ?></h1>
					<div class="section-global-header-wrap clearfix">
						<div class="section-global-header clearfix">
							<div class="white-area">
								<div class="container">
									<div class="main-content row">
										<?php echo($home_post_obj->post_content); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
	    		</div>
			</section>
		</div>
		<?php
		} else {		
		?>
		<?php 
		//$post = $wp_query->post;
		//$page_post_obj = get_post( $post->ID ); 
		?>

		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>    
		

    	<div id="page-content">

    		<section id="home" class="home-sections">
	    		<div class="section-content">					
					<h1 class="page-top-title"><?php echo the_field('page_title'); ?></h1>
					<div class="section-global-header-wrap clearfix">
						<div class="section-global-header clearfix">
							<div class="white-area">
								<div class="container">
									<div id="tasting_report" class="main-content row">
										<div id="current_entry_ids" style="display:none">
											<?php echo FrmProEntriesController::entry_link_shortcode_simple(array('id' => 18, 'field_key' => 'eventidcalendar')); ?>
										</div>
										<?php 
											if(isset($_GET['d']))
											{
											    // Do something
											} else {
												header('Location: http://setxmarketing.com/rep-portal-area/');
											}
											?>
										<?php $eventID = base64_decode($_GET["d"]) ?>
										<?php $event_post = get_post( $eventID, ARRAY_A );?>
										<?php $address_post = base64_decode($_GET["l"]) ?>
											<?php //print_r($location_post);
											?>
											
											<?php
												$product_list = get_field( "tasting_product_1", $eventID);
												
												if ( get_field( "tasting_product_2", $eventID ) != "None" ) { 
													$product_list .= ', '.get_field( "tasting_product_2", $eventID );
												}
												if ( get_field( "tasting_product_3", $eventID ) != "None" ) { 
													$product_list .= ', '.get_field( "tasting_product_3", $eventID );
												}
												if ( get_field( "tasting_product_4", $eventID ) != "None" ) { 
													$product_list .= ', '.get_field( "tasting_product_4", $eventID );
												}	
											?>
											<input type="hidden" id="event_id" value="<?php echo $eventID ?>">
											<input type="hidden" id="event_title_hidden" value="<?php echo get_the_title($eventID) ?>">	
											<input type="hidden" id="list_brand_hidden" value="<?php echo $product_list ?>">
											<input type="hidden" id="event_location_hidden" value="<?php echo get_the_title(get_post_meta($eventID, 'pec_location', true)) ?>">	
											<input type="hidden" id="event_location_address_hidden" value="<?php echo $address_post ?>">	
											
											<input type="hidden" id="event_date_hidden" value="<?php echo date('F d, Y', strtotime(substr(get_post_meta($eventID, 'pec_date', true), 0, 10))); ?>">
											<input type="hidden" id="product_1_hidden" value="<?php echo get_field( "tasting_product_1", $eventID); ?>">
											
											<input type="hidden" id="product_2_hidden" value="<?php echo get_field( "tasting_product_2", $eventID); ?>">
											<input type="hidden" id="product_3_hidden" value="<?php echo get_field( "tasting_product_3", $eventID); ?>">
											<input type="hidden" id="product_4_hidden" value="<?php echo get_field( "tasting_product_4", $eventID); ?>">
											
											<?php
												$hour = substr(get_post_meta($eventID, 'pec_date', true), 11, 2);									
												$ampm = date('a', mktime($hour, 0));
												$hour = $hour > 12 ? $hour - 12 : ($hour == '00' ? '12' : $hour);
												$end_time_hh = get_post_meta($eventID, 'pec_end_time_hh', true);
												$end_time_mm = get_post_meta($eventID, 'pec_end_time_mm', true);
												$ampm_end = date('a', mktime($end_time_hh, 0));
												$endhour = $end_time_hh > 12 ? $end_time_hh - 12 : ($end_time_hh == '00' ? '12' : $end_time_hh);
											?>
											<input type="hidden" id="event_time_hidden" value="<?php echo $hour.':'.substr(get_post_meta($eventID, 'pec_date', true), 14, 2).' '.$ampm.' - '.$endhour.':'.$end_time_mm.' '.$ampm_end; ?>">
												
										<div class="tasting-report-beta">
											
											<h1 class="tasting-report-title"><section>Tasting Summary - <span><?php echo get_the_title(get_post_meta($eventID, 'pec_location', true)) ?></span></section></h1>
										
										
											<?php the_content(); ?>
										</div>
										<?php //$event_data = DpProEventCalendar->getEventData($eventID); print_r($event_data); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
	    		</div>
			</section>
		</div>


		<?php endwhile; ?>
		<?php endif; ?>


		<?php
		}		
		?>
    </div>
</body>
</html>
