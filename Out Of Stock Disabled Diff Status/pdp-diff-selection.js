createCombinationListForDisabledEnabledList(_this, ) {
    // needs to occur after JSON Response
    var combinationListForDisabledEnabledObj = {};

    for (var i=0; i<_this.parentListFromFirstAListArr.length; i++) {
        var currentComboList = this.comboList[`${this.parentListFromFirstAListArr[i]}`];

        var selectedComboList = new Array();
        jQuery.each( currentComboList, function( key, value ) {
            var diffCombo = "";
            jQuery.each( value, function( k, v ) {
                diffCombo += v;
                if (k<value.length-1)
                    diffCombo += "-";
            });
            selectedComboList.push(`${diffCombo}`);
            diffCombo = "";
        });
        combinationListForDisabledEnabledObj[`${this.parentListFromFirstAListArr[i]}`] = selectedComboList;
    }

    for (var v in combinationListForDisabledEnabledObj) {
        var disableArr = new Array();
        var enableArr = new Array();
        jQuery.each(combinationListForDisabledEnabledObj[v], function (key, value) {
            var combo = PdpDiffSelection.skuDiffCombinations[`pdp_product-${value}`];
            if (combo != undefined) {
                if (combo.inventoryQty > 0) {
                    enableArr.push(value);
                } else {
                    disableArr.push(value);
                }
            } else {
                disableArr.push(value);
            }
        });
        this.disableList[v] = disableArr;
        this.enableList[v] = enableArr;
    }
}

diffSelectionEventsDisableDiffs(o) {
    // create list that shows which items are disabled
    var disableArr = this.disableList[this.currentParentAttributeIdChecked];
    var enableArr = this.enableList[this.currentParentAttributeIdChecked];

    // depending on group count need to compare if disable diff is available in enable diff if yes dont disable
    var allLength = this.attributeListArr.length;
    for (var i = 0; i < this.attributeListArr.length; i++) {
        jQuery(this.attributeListArr[i]).find("a").each(function() {
            var disable = PdpDiffSelection.checkIfInEnableDisable(jQuery(this), jQuery(this).data("id"), allLength, i, disableArr, enableArr);
            if(disable[1] == false) {
                jQuery("#rpdp-product-size-selection").find(`[data-id='${disable[0]}']`).addClass("disabled");
            } else {
                jQuery("#rpdp-product-size-selection").find(`[data-id='${disable[0]}']`).removeClass("disabled");
            }
        })
    }
    // check parents for full disable
    for (var i = 0; i < this.parentListFromFirstAListArr.length; i++) {
        var disable = PdpDiffSelection.checkIfInEnableDisable(jQuery(`a[data-id=${this.parentListFromFirstAListArr[i]}]`), `${this.parentListFromFirstAListArr[i]}`, allLength, i, disableArr, enableArr, "parent");
        if(disable[1] == false) {
            jQuery("#rpdp-product-size-selection").find(`[data-id='${disable[0]}']`).addClass("disabled");
        } else {
            jQuery("#rpdp-product-size-selection").find(`[data-id='${disable[0]}']`).removeClass("disabled");
        }
    }
    this.preSelectionFlag = false;
}

checkIfInEnableDisable(obj, objId, all, index, disableArr, enableArr, version) {
    /*
    console.debug(
        `
        objId: ${objId}
        all: ${all+1}
        index : ${index+1}
        disableArr: ${disableArr}
        enableArr: ${enableArr}
        version: ${version}
        originalAttributeListArr: ${this.originalAttributeListArr.length}
        `
    );
    */
    var disableObj = new Array(`${objId}`),
        isThisInBoth = false,
        subParentDiff = "",
        index = index+1,
        currentCombo;

    if (index == 1) {
        subParentDiff = PdpDiffSelection.currentParentAttributeIdChecked;
        currentCombo = `${subParentDiff}-${objId}`;
    } else {
        jQuery(PdpDiffSelection.originalAttributeListArr[index-1]).find("a").each(function () {
            if (jQuery(this).hasClass("checked")) {
                subParentDiff = jQuery(this).data("id");
                currentCombo = `${subParentDiff}-${objId}`;
                // do we need to tag a SubSubParent?
                if (index > 1 ) {
                    // which subparent do we need?
                    console.debug("which do we need? ", index);
                    jQuery(PdpDiffSelection.originalAttributeListArr[index-2]).find("a").each(function () {
                        if (jQuery(this).hasClass("checked")) {
                            let subsubParentDiff = jQuery(this).data("id");
                            console.debug("subsubParentDiff: ", subsubParentDiff);
                            currentCombo = `${subsubParentDiff}-${currentCombo}`;
                        }
                    })
                }
                return false;
            }

        });
    }

    // the idea is that more diffs need to be added as the index number increases

    if (version != "parent") {
        for (var i = 0; i < disableArr.length; i++) {
            //console.debug("disableArr[i].indexOf(`${subParentDiff}-${objId}`): ", disableArr[i].indexOf(`${subParentDiff}-${objId}`));
            if (disableArr[i].indexOf(`${currentCombo}`) != -1) {
                isThisInBoth = false;
                break;
            } else {
                isThisInBoth = false;
            }
        }

        for (var i = 0; i < enableArr.length; i++) {
            //console.debug("enableArr[i].indexOf(`${subParentDiff}-${objId}`): ", enableArr[i].indexOf(`${subParentDiff}-${objId}`));
            if (enableArr[i].indexOf(`${currentCombo}`) != -1) {
                isThisInBoth = true;
                break;
            } else {
                isThisInBoth = false;
            }
        }
    }
    else {
        var enableListLength = this.enableList[objId].length;
        if (enableListLength>0) {
            for (var i = 0; i < this.enableList[objId].length; i++) {
                //console.debug("parent this.enableList[objId][i].indexOf(`${objId}`): ", this.enableList[objId][i].indexOf(`${objId}`));
                if (this.enableList[objId][i].indexOf(`${objId}`) != -1) {
                    isThisInBoth = true;
                    break;
                } else {
                    isThisInBoth = false;
                }
            }
        }
        else {
            isThisInBoth = false;
        }
    }

    console.debug(`current combo: ${currentCombo}`);
    disableObj.push(isThisInBoth);
    return disableObj;
}