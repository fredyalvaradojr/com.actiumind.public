<?php
/*
Template Name: Frontpage
*/
?>
<?php get_header(); ?>

<div id="feature-items" class="container">
	<div class="row">
		<div class="feature-items-item col-sm-4">
			<div class="feature-items-item-inner">
				<img src="<?php echo get_template_directory_uri();?>/img/sweet-galleries.jpg">
				<h2 class="feature-items-item-title">SWEEET GALLERIES</h2>
				<p class="feature-items-item-description">It's time for some slumber party fun!</p>
				<a href="/galleries" class="feature-items-item-viewmore">View More <img src="<?php echo get_template_directory_uri();?>/img/view-more-arrow.svg"></a>
			</div>
		</div>
		<div class="feature-items-item col-sm-4">
			<div class="feature-items-item-inner">
				<img src="<?php echo get_template_directory_uri();?>/img/featured-galleries.jpg">
				<h2 class="feature-items-item-title">SWEEET PACKAGES</h2>
				<p class="feature-items-item-description">choose your slumber package to pair with our sweeet teepees and we will take care, of good memories, delivery, set-up and styling.</p>
				<a href="/packages" class="feature-items-item-viewmore">View More <img src="<?php echo get_template_directory_uri();?>/img/view-more-arrow.svg"></a>
			</div>
		</div>
		<div class="feature-items-item col-sm-4">
			<div class="feature-items-item-inner">
				<img src="<?php echo get_template_directory_uri();?>/img/featured-theme-invites.jpg">
				<h2 class="feature-items-item-title">THEME INVITES</h2>
				<p class="feature-items-item-description">unique designs with elegance and beauty making a lasting impression no matter what party you choose!</p>
				<a href="/packages#theme-invites" class="feature-items-item-viewmore">View More <img src="<?php echo get_template_directory_uri();?>/img/view-more-arrow.svg"></a>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>