	</div><!--/.container-->
	
	<footer id="footer">
		<div id="home-general-info">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
					<a href="/packages#theme-kits"><img src="<?php echo get_template_directory_uri();?>/img/sweeet-invite.png"></a>
					</div>
					<div class="col-sm-9">
						<h3 class="general-info-title">ABOUT US</h3>
						<p>Sweeet Parties is a slumber party specialist serving Cypress, The Woodlands and Katy. By taking care of all the details we can provide a stress free experience full of fun and endless imagination.</p>
						<p>Our packages and themes will create a unique and special party that will create memories that will be remembered for years to come.</p>
						<p>Teepees are a fun and entertaining way for children to create a spot where they can let their imaginations run wild. Also, you can choose from a range of theme kits that will allow the little partygoers to have creative moments with addtional fun.</p>
                		<p>Sweet Parties will take special care of every detail so that all you have to do is enjoy the unique experinece and see how the little partygoers enjoy every moment and details.</p>            
					</div>
				</div>
			</div>
		</div>
		<div id="footer-information">
			<nav id="footer-nav">
				<a href="https://www.instagram.com/sweeetparties/">INSTAGRAM</a> <a href="/wp-content/uploads/2017/05/termsandconditions.pdf">TERMS AND CONDITIONS</a>
			</nav>

			<div id="footer-location">
				<span class="footer-location-address"></span> <span rel="tel" class="footer-location-phone"></span> <a href="mailto:info@sweeetparties.com" class="footer-location-email"><span>info@sweeetparties.com</span></a>
			</div>

			<div id="footer-faux-logo">
				<img src="<?php echo get_template_directory_uri();?>/img/footer-faux-logo.svg">
			</div>
		</div>
		
	</footer><!--/#footer-->

</div><!--/#wrapper-->

<?php wp_footer(); ?>
</body>
</html>