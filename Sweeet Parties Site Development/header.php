<!DOCTYPE html> 
<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<?php wp_head(); ?>
	<script src="https://use.fontawesome.com/0480abc291.js"></script>
</head>

<body <?php body_class(); ?>

<div id="wrapper">

	<header id="header" class="group">

		<?php if ( has_nav_menu('topbar') ): ?>
			<nav class="nav-container group" id="nav-topbar">
				<div id="social-info-bar">
					<div class="container-fluid">
				 		<div class="row">
				 			<div class="col-sm-6">
				 				<a href="mailto:info@sweeetparties.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				 				<a href="mailto:info@sweeetparties.com"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				 			</div>
				 			<div class="col-sm-6 text-align-right">
				 				<a href="mailto:info@sweeetparties.com"><i class="fa fa-envelope" aria-hidden="true"></i> info@sweeetparties.com</a>
				 				<a href="/book-now"><i class="fa fa-info-circle" aria-hidden="true"></i> Book Now!</a>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div id="header-menu-area" class="container-fluid">
					<div class="row no-gutters">
						<div id="header-menu-area-menus" class="col-sm-12">
							<div id="logo-header-container">
							<img id="logo-header" src="<?php echo get_template_directory_uri();?>/img/sweeetparties-logo-bg.svg">
							<img id="logo-header-no-bg" src="<?php echo get_template_directory_uri();?>/img/sweeetparties-logo-nobg.svg">
							</div>
							<div class="nav-toggle"><i class="fa fa-bars"></i></div>
							<div class="nav-text">
								<?php wp_nav_menu(array('theme_location'=>'topbar','menu_class'=>'nav container-inner group','container'=>'','menu_id' => '','fallback_cb'=> false)); ?>
							</div>
							<div class="nav-wrap">
								<div id="tagline-header">
									<?php wp_nav_menu(array('theme_location'=>'topbar','menu_class'=>'nav container-inner group','container'=>'','menu_id' => '','fallback_cb'=> false)); ?>
								</div>
								<p id="header-menu-tag">Sweeet parties provides sweeet experiences that your kids will remember for many years to come.</p>
							</div>
						</div>
					</div>
				</div>
				
				<!--
				<div class="container">
					<div class="container-inner">		
						<div class="toggle-search"><i class="fa fa-search"></i></div>
						<div class="search-expand">
							<div class="search-expand-inner">
								<?php get_search_form(); ?>
							</div>
						</div>
					</div><!--/.container-inner-->
					<!--
				</div><!--/.container-->
				
			</nav><!--/#nav-topbar-->
		<?php endif; ?>
		
	</header><!--/#header-->
	<?php 
	if ( is_front_page() ) { ?>
	<div id="content-header-featurearea">
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/paris-b.jpg"></div>
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/paw-b.jpg"></div>
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/spring-b.jpg"></div>
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/pamper-b.jpg"></div>
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/nina-b.jpg"></div>
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/huntig-b.jpg"></div>
		<div><img src="<?php echo get_template_directory_uri();?>/img/home/dino-b.jpg"></div>
	</div>
	<?php
	}
	?>

	<div id="page">