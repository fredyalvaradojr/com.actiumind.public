import React from 'react';
import axios from 'axios';
import helperFunctions from './../helperJS/helperFunctions';
import FlagSkyCanvas from './Modules/FlagSkyCanvas.jsx';
import FlagTitleStatus from './Modules/FlagTitleStatus.jsx';
import Flag from './Modules/Flag.jsx';
import FlagStatusInformation from './Modules/FlagStatusInformation.jsx';
import SocialSharing from './Modules/SocialSharing.jsx';
import CopyInformation from './Modules/CopyInformation.jsx';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      flagTitleStatus: '',
      flagContentTitle: '',
      flagContent: 'null',
      flagContentDate: '',
      flagMastAnimation: ''
    };
  }
  componentDidMount() {
    const currentYear = helperFunctions.getCurrentYear();
    const monthName = helperFunctions.getCurrentMonthName();
    const day = helperFunctions.getCurrentDay();
    const Calendar = `https://usflagtoday.firebaseio.com/Calendar/${currentYear}/${monthName}/${day}.json`;
    axios.get(Calendar)
      .then((res) => {
        const currentStatusMessage = !(res.data === null) ? 'Why is the flag half mast today?' : 'Nothing to report Today...';
        const currentStatusContent = !(res.data === null) ? res.data.content : 'null';
        const currentStatusContentTitle = !(res.data === null) ? res.data.title : 'Have a great day!';
        const currentflagContentDate = !(res.data === null) ? res.data.date : helperFunctions.getFullDate();
        const currentFlagMastAnimation = !(res.data === null) ? 'flag__icon-cloth__half-mast' : 'flag__icon-cloth__full-mast';

        this.setState({
          flagTitleStatus: currentStatusMessage,
          flagContent: currentStatusContent,
          flagContentTitle: currentStatusContentTitle,
          flagContentDate: currentflagContentDate,
          flagMastAnimation: currentFlagMastAnimation
        });

        // turn off preload
      });
  }
  render() {
    return (
      <div className="flag">
        <section className="flag__section flag__sky">
          <FlagSkyCanvas />
          <FlagTitleStatus title={this.state.flagTitleStatus} />
          <Flag flagMastAnimation={this.state.flagMastAnimation} />
        </section>
        <section className="flag__section flag__grass">
          <FlagStatusInformation flagContentDate={this.state.flagContentDate} flagContent={this.state.flagContent} flagContentTitle={this.state.flagContentTitle} />
          <SocialSharing />
          <CopyInformation />
        </section>
      </div>
    );
  }
}

export default App;
