class pdpDiffSelection {

    // Class attributes
    constructor() {
        this.productDisplayPageSelectable = document.querySelector("#productDisplayPageSelectable");
        this.setDiffProductType = this.checkTypeOfProduct();
        this.catalogEntryID = document.querySelector("#catalog-entry-id");
        this.hashValue = location.hash;
        this.selectableProduct = true;
        this.preSelectionFlag = true;
        this.productExists = true;
        this.localStoreInventoryInit = true;
        this.currentAdBugType = "";
        this.currentPartNumber = "";
        this.currentCatEntryId = "";
        this.currentOfferPrice = "";
        this.hashType = "";
        this.hashKey = "";
        this.hashCatentryID = "";
        this.hashIdLocateDiffCombination = "";
        this.activeCarousel = false;
        this.productJsonView = {};
        this.skuDiffCombinations = {};
        this.diffCombinationInformation = {};
        this.srUnitMeasure();
        this.srUnitEcho();
    }

    //  Accessor methods
    get diffProductType() {
        return this.productDiffSelectable;
    }

    set setDiffProductType(v) {
        this.productDiffSelectable = v;
    }

    checkTypeOfProduct() {
        if (this.productDisplayPageSelectable != null) {
            return this.productDisplayPageSelectable.getAttribute("data-selectable"); // meaning there are no diffs to select
        } else {
            return "selectable"; // meaning there are one or more diffs to select
        }
    }

    whichTypeofHashAreYou() {
        var checkType = this.hashValue.substring(1);
        if (checkType.substring(0, 14) === "repChildCatid=") {
            this.hashType = "catentry_id";
            this.hashKey = "sku-diff_childCatID";
            this.hashCatentryID = checkType.split("=")[1];
            this.hashIdLocateDiffCombination = `child_cat_id-${checkType.split("=")[1]}`;
        } else if (checkType.substring(0, 15) === "repChildCatSku=") {
            this.hashType = "partnumber";
            this.hashKey = "sku-diff_childCatSku";
            this.hashCatentryID = checkType.split("=")[1];
            this.hashIdLocateDiffCombination = `child_cat_sku-${checkType.split("=")[1]}`;
        } else {
            this.hashType = "noPreference";
        }
    }

    buildAdbugNode(n, cl, t) {
        n.removeAttribute("class", "");
        n.className = cl;
        n.innerHTML = t;
    }

    diffSelectionEventsCheckedNoDiffs() {
        var diffExistArr = new Array();
        jQuery(".atrribute-list").each( function() {
            diffExistArr.push( jQuery(this).find("a") );
        });
        return diffExistArr.length;
    }

    diffSelectionEventsChecked(_arr, _o) {
        var checkNoDiffs = this.diffSelectionEventsCheckedNoDiffs();
        if ( ( this.selectableProduct || checkNoDiffs > 0 ) && _arr.length > 0) {
            for (let i = 0; i < _arr.length; i++) {
                var diffAttributeSelectArea = document.querySelector("#rpdp-product-size-selection");
                if (diffAttributeSelectArea != null) {
                    var diffAttributeNode = diffAttributeSelectArea.querySelectorAll(`[data-id="${_o.Attributes[_arr[i]]}"]`);
                    var diffAttributeNodeJ = jQuery(`[data-id="${_o.Attributes[_arr[i]]}"]`);
                    /*if (diffAttributeNode != 'undefined') {
                        diffAttributeNode[0].checked = true;
                        // populate attrValueOf_property text
                        var diffAttributeNodeValue = diffAttributeNode[0].getAttribute("value");
                        var diffAttributeNodeParentGroupTitle = diffAttributeNode[0].getAttribute("name");
                        var diffAttributeNodeParentGroupTitleNode = document.getElementById(`attrValueOf_${diffAttributeNodeParentGroupTitle}`);
                        diffAttributeNodeParentGroupTitleNode.innerHTML = diffAttributeNodeValue;
                    }*/
                    // anchor tag
                    if (diffAttributeNodeJ != 'undefined') {
                        diffAttributeNodeJ.addClass('checked');
                        // populate attrValueOf_property text
                        var diffAttributeNodeValue = diffAttributeNodeJ.data("value");
                        var diffAttributeNodeParentGroupTitle = diffAttributeNodeJ.attr("name");
                        var diffAttributeNodeParentGroupTitleNode = document.getElementById(`attrValueOf_${diffAttributeNodeParentGroupTitle}`);
                        diffAttributeNodeParentGroupTitleNode.innerHTML = diffAttributeNodeValue;
                    }
                }
            }
        } else {
            jQuery(".attribute-group").each(function() {
                jQuery(this).find("h3").find("span").text("");
            })
        }
    }

    diffSelectionEventsPricingMAP(n, lp, op, mp, wasNow) {
        if (op < mp) {
            let formatedPrice = GeneralComponentsClass.numberFormat(mp);
            n.innerHTML = `
                <h2 id="offerPrice_${this.catalogEntryID}" class="price see_cart z-pricing z-pricing-special z-pricing-map">
                   Our Price in Cart <span id="mapPrice">Compare at $${formatedPrice}</span>
                </h2>
                <div class="sr-only" id="offerPrice_update_${this.catalogEntryID}" aria-live="polite" aria-atomic="true">
                    Price has updated to: Our Price in Cart <span id="mapPrice">Compare at $${formatedPrice}
                </div>
                `;

        } else if (wasNow === "true") {
            this.diffSelectionEventsPricingWasNow(n, op, lp);
        } else {
            let formatedPrice = GeneralComponentsClass.numberFormat(op);
            n.innerHTML = `
                <h2 id="offerPrice_${this.catalogEntryID}" class="price priceCurr2">
                    <span id="currentPrice" class="currentPrice">$${formatedPrice}</span>
                </h2>
                <div class="sr-only" id="offerPrice_update_${this.catalogEntryID}" aria-live="polite" aria-atomic="true">
                    Price has updated to: $${formatedPrice}
                </div>
                `;
        }
    }

    diffSelectionEventsPricingWasNow(n, op, lp) {
        if (op < lp) {
            let formatedOfferPrice = GeneralComponentsClass.numberFormat(op);
            let formatedListPrice = GeneralComponentsClass.numberFormat(lp);
            n.innerHTML = `
                <h2 id="offerPrice_${this.catalogEntryID}" class="z-pricing z-pricing-special">
                    <span id="currentPrice" class="currentPrice">$${formatedOfferPrice}</span><span> was $${formatedListPrice}</span>
                </h2>
                <div class="sr-only" id="offerPrice_update_${this.catalogEntryID}" aria-live="polite" aria-atomic="true">
                    Price has updated to: $${formatedOfferPrice} was $${formatedListPrice}
                </div>
                `;
        } else {
            this.diffSelectionEventsPricingRegular(n, op);
        }
    }

    diffSelectionEventsPricingRegular(n, op) {
        let formatedPrice = GeneralComponentsClass.numberFormat(op);
        n.innerHTML = `
            <h2 id="offerPrice_${this.catalogEntryID}" class="price priceCurr2">
                <span id="currentPrice" class="currentPrice">$${formatedPrice}</span>
            </h2>
            <div class="sr-only" id="offerPrice_update_${this.catalogEntryID}" aria-live="polite" aria-atomic="true">
                Price has updated to: $${formatedPrice}
            </div>
            `;
    }

    diffSelectionEventsPricing(o) {
        var pricingUpdateArea = document.querySelector("#pricing-update-area"),
            showWasNow = o.showWasNow,
            mapFlag = o.map_flag,
            listPrice = parseFloat(o.listPrice),
            offerPrice = parseFloat(o.offerPrice),
            mapPrice = parseFloat(o.mapPrice);
        if (pricingUpdateArea != null) {
            pricingUpdateArea.innerHTML = "";
            GeneralComponentsClass.removeClass(pricingUpdateArea, "visibility-hidden");
        }
        if (mapFlag === "Y") {
            this.diffSelectionEventsPricingMAP(pricingUpdateArea, listPrice, offerPrice, mapPrice, showWasNow);
        } else {
            if (showWasNow === "true") {
                this.diffSelectionEventsPricingWasNow(pricingUpdateArea, offerPrice, listPrice);
            } else {
                this.diffSelectionEventsPricingRegular(pricingUpdateArea, offerPrice);
            }
        }
    }

    diffSelectionEventsChangeSkuItemNumbers(o) { //dont understand why sku is partNumber
        if (o["no-sku"] == undefined) {
            let node = document.querySelector('[id^="product_SKU"]');
            if (node != null)
                node.innerHTML = `SKU: ${o.partNumber}`;
        } else {
            let node = document.querySelector('[id^="product_SKU"]');
            if (node != null)
                node.innerHTML = `SKU: N/A`;
        }
    }

    diffSelectionEventsChangeManufacturingPartNumber(o) {
        if (o["no-sku"] == undefined) {
            let node = document.querySelector('[id^="product_MFPARTNUMBER"]');
            if (node != null)
                node.innerHTML = `Item: ${o.mfPartNumber}`;
        } else {
            let node = document.querySelector('[id^="product_MFPARTNUMBER"]');
            if (node != null)
                node.innerHTML = `Item: N/A`;
        }
    }

    srUnitMeasure() {
        var thisClassName;
        jQuery.each(jQuery('.unitMeasure'),function(i,v){
            var a = jQuery(this).text().split("");
            jQuery.each(a, function (key, val) {
                // search for value and replace it
                a[key] = val.replace("'","'feet ").replace('"','"inches');
            });
            var b = a.join('');

            jQuery(this).text(b);

            thisClassName = jQuery(this).attr('class');

        });
    }

    srUnitEcho() {
        var thisClassName = jQuery('.unitMeasure').attr('class');
        //search for value and replace it back
        jQuery('.'+thisClassName+':contains("feet")').html(function(index, oldHTML) {
            return oldHTML.replace(/(feet)/g, '<span class="sr-only">$&</span>');
        });
        jQuery('.'+thisClassName+':contains("inches")').html(function(index, oldHTML) {
            return oldHTML.replace(/(inches)/g, '<span class="sr-only">$&</span>');
        });
    }

    diffSelectionEventsCheckAttributeListTotal() {
        var groupCount = 0;
        var pageName = document.querySelector("meta[name='pageName']");
        if (pageName != null) {
            if (pageName.getAttribute("content") === "BundlePage") {
                /*jQuery('.rpdp-bundle-component-attributes').each(function() {
                    jQuery(this).find('.atrribute-list').each( function() {
                        var liGroupCount = jQuery(this).find('li').length;
                        if(liGroupCount < 2) {
                            jQuery(this).addClass("hide");
                            jQuery(this).parent().find("h3").html(function(index,html) {
                                return html.replace('Select ','');
                            });
                            return;
                        } if(liGroupCount < 3 && jQuery(".rpdp-product-size-selection-clearance-titles") && groupCount == 0) {
                            jQuery(".rpdp-product-size-selection-clearance-title").parent().addClass("hide");
                            jQuery(".rpdp-product-size-selection-clearance-title").parent().parent().find("h3").html(function(index,html) {
                                return html.replace('Select ','');
                            });
                        }
                        groupCount++;
                    });
                });*/
            } else {
                jQuery('#rpdp-product-size-selection').find('.atrribute-list').each( function() {
                    var diffCount = jQuery(this).find('a').length;
                    if(diffCount < 2) {
                        jQuery(this).addClass("hide");
                        jQuery(this).parent().find("h3").html(function(index,html) {
                            return html.replace('Select ','');
                        });
                        return;
                    }
                });
            }
        }
    }

    diffSelectionEventsAdBugAvailability(o) {
        if (o["no-sku"] == undefined) {
            var adBugType = o.PromotionAdBug.replace('.png', ''),
                node = document.getElementsByClassName('flag-bug')[0];
            this.currentAdBugType = adBugType;
            if (node != null && adBugType.length > 0) {
                // choose the right adbug to switch
                switch (adBugType) {
                    case "adbug-clearance":
                        this.buildAdbugNode(node, "promoview promoview-clearance flag-bug", "Clearance");
                        break;
                    case "adbug-hotdeal":
                        this.buildAdbugNode(node, "promoview promoview-hotdeal flag-bug", "Hot Deal");
                        break;
                    case "adbug-pricedrop":
                        this.buildAdbugNode(node, "promoview promoview-pricedrop flag-bug", "Price Drop");
                        break;
                    case "adbug-new":
                        this.buildAdbugNode(node, "promoview promoview-new flag-bug", "New");
                        break;
                }
            } else {
                // there is no adbug, clear and hide node
                if (node != null) {
                    node.innerHTML = "";
                    GeneralComponentsClass.addClass(node, "hide");
                }
            }
        } else {
            // there is no adbug, clear and hide node
            node = document.getElementsByClassName('flag-bug')[0];
            if (node != null) {
                node.innerHTML = "";
                GeneralComponentsClass.addClass(node, "hide");
            }
        }
    }

    diffSelectionEventproductImageViewPopulateNewImages(ai, pic, tcc, alt, pi, tic) {
        if (ai["no-sku"] == undefined) {
            var thumbJump;
            var parentImagesContent = "<ul>";
            var alternateImagesContent = "<ul>";
            for (var i = 0; i < ai.length; i++) {
                if(i==0) {
                    thumbJump = `${ai[i]}`;
                } else {
                    thumbJump = `${ai[i]}?is=150,150`;
                }
                parentImagesContent +=
                    `
                <li id="rpdp-product-hero-image-large-${i}">
                    <img src="${ai[i]}" itemprop="image" alt="${alt} - view number ${i+1}">
                </li>
                `;
                alternateImagesContent +=
                    `
                <li id="rpdp-product-hero-image-large-carousel-${i}">
                    <span class="slide-state">
                        <img src="${thumbJump}" itemprop="image" alt="${alt} - view number ${i+1}">                 
                    </span>
                </li>
                `;
            }
            parentImagesContent += "</ul>";
            alternateImagesContent += "</ul>";

            tic.html(alternateImagesContent);
        } else {
            var parentImagesContent = "<ul>";
            parentImagesContent +=
                `
                <li id="rpdp-product-hero-image-large-0">
                    <img src="${ai['default-image']}" itemprop="image" alt="${alt} - view number 1">
                </li>
                `;
            parentImagesContent += "</ul>";
        }

        pi.html(parentImagesContent);

    }

    diffSelectionEventproductImageViewCreateCarouselsActivate(ai) {
        var _this = this;
        // is there more than 0 alternate views
        if (ai.length > 0) {
            var largeImageCarous = jQuery("#rpdp-product-hero-image-large.carous");
            var thumbImageCarous = jQuery("#rpdp-product-hero-image-large-carousel.carous");
            var thumbImageCarousWrap = jQuery("#rpdp-product-hero-image-large-carousel-wrap");
            var navPrev = thumbImageCarousWrap.find(".rpdp-product-hero-image-large-carousel-nav-prev");
            var navNext = thumbImageCarousWrap.find(".rpdp-product-hero-image-large-carousel-nav-next");

            largeImageCarous
                .on('jcarousel:reload jcarousel:create', function () {
                    var carousel = jQuery(this),
                        width = carousel.innerWidth();
                    carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                })
                .on('jcarousel:createend', function (event, carousel) {
                    GeneralComponentsClass.removeClass(carousel._element[0], "init");
                    let preSpinner = document.querySelector("#rpdp-spa-loader");
                    if (preSpinner != null) {
                        setTimeout(function () {
                            preSpinner.outerHTML = "";
                            jQuery(".rpdp-product-hero-image-large-carousel-nav-prev").click();
                        }, 300);
                    }
                })
                .jcarousel()
                .jcarouselSwipe();

            thumbImageCarous
                .on('jcarousel:reload jcarousel:create', function () {
                    var carousel = jQuery(this),
                        width = thumbImageCarous.innerWidth();

                    if (width >= 528) {
                        width = (width) / 6;
                    } else {
                        width = (width) / 4;
                    }
                    carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                })
                .jcarousel()
                .jcarouselSwipe();

            function carousConnector(itm, lic) {
                return lic.jcarousel('items').eq(itm.index());
            }

            thumbImageCarous
                .jcarousel('items').each(function () {
                var item = jQuery(this);

                // This is where we actually connect to items.
                var target = carousConnector(item, largeImageCarous);

                item
                    .on('jcarouselcontrol:active', function () {
                        thumbImageCarous.jcarousel('scrollIntoView', this);
                        item.addClass('active');
                        jQuery("#rpdp-product-hero-image-large-update").html(jQuery(this).find("img").attr('alt'));
                        //check if spin is active
                        _this.disableInvodoSpin();
                    })
                    .on('jcarouselcontrol:inactive', function () {
                        item.removeClass('active');
                    })
                    .jcarouselControl({
                        target: target,
                        carousel: largeImageCarous
                    });
            });

            // Setup controls for the navigation carousel
            navPrev
                .on('jcarouselcontrol:inactive', function () {
                    jQuery(this).addClass('inactive').prop("disabled", true);

                })
                .on('jcarouselcontrol:active', function () {
                    jQuery(this).removeClass('inactive').prop("disabled", false);
                })
                .jcarouselControl({
                    target: '-=1',
                    carousel: largeImageCarous
                });

            navNext
                .on('jcarouselcontrol:inactive', function () {
                    jQuery(this).addClass('inactive').prop("disabled", true);
                })
                .on('jcarouselcontrol:active', function () {
                    jQuery(this).removeClass('inactive').prop("disabled", false);
                })
                .jcarouselControl({
                    target: '+=1',
                    carousel: largeImageCarous
                });
        }
    }

    diffSelectionEventproductImageViewCreateCarousels(ai, pic, tcc, alt, same, pi, tic) {
        var jcarAttr = pi.data('jcarousel');
        if (typeof jcarAttr !== typeof undefined && jcarAttr !== false) {
            pi
                .jcarousel('destroy')
                .jcarouselSwipe('destroy')
                .addClass("init");
            tic
                .jcarousel('destroy')
                .jcarouselSwipe('destroy');
        }
        this.diffSelectionEventproductImageViewCreateCarouselsActivate(ai);
    }

    diffSelectionEventProductImageView(o) {
        // get the current parent image from the dom
        var parentImages = jQuery("#rpdp-product-hero-image-large");
        var parentImagesChildren = jQuery("#rpdp-product-hero-image-large ul");
        var parentImagesCurrentImageSrc = jQuery("#rpdp-product-hero-image-large ul").find("img:first").attr("src");
        var alternateImages = o.alternateImages;
        var thumbImageCarousel = jQuery("#rpdp-product-hero-image-large-carousel");
        var thumbImageCarouselWap = jQuery("#rpdp-product-hero-image-large-carousel-wrap");
        var thumbImageCarouselChildren = jQuery("#rpdp-product-hero-image-large-carousel ul");
        var alternativeText = document.querySelector(`#PageHeading_${this.catalogEntryID}`).innerText;

        if (o["no-sku"] == undefined) {
            // add the parent image to the list of alternate images

            // check if the image src is different
            if (parentImagesCurrentImageSrc != o.parentImage) {
                this.diffSelectionEventproductImageViewPopulateNewImages(alternateImages, parentImagesChildren, thumbImageCarouselChildren, alternativeText, parentImages, thumbImageCarousel);
                this.diffSelectionEventproductImageViewCreateCarousels(alternateImages, parentImagesChildren, thumbImageCarouselChildren, alternativeText, false, parentImages, thumbImageCarousel);
            } else {
                // are there more than 1 alternate images?
                // possible that product is coming from a not available one image view
                // check if the list has the same amount of images and ai.length
                if (alternateImages.length != parentImagesChildren.length) {
                    // not the same you need to populate new images and reset the carousel
                    this.diffSelectionEventproductImageViewPopulateNewImages(alternateImages, parentImagesChildren, thumbImageCarouselChildren, alternativeText, parentImages, thumbImageCarousel);
                }
                if (alternateImages.length > 1) {
                    this.diffSelectionEventproductImageViewCreateCarousels(alternateImages, parentImagesChildren, thumbImageCarouselChildren, alternativeText, true, parentImages, thumbImageCarousel);
                }
            }

            //display carousel check
            if (alternateImages.length < 2) {
                thumbImageCarouselWap.addClass("visibility-hidden");
                let preSpinner = document.querySelector("#rpdp-spa-loader");
                if (preSpinner != null) {
                    setTimeout(function () {
                        preSpinner.outerHTML = "";
                    }, 300);
                }
            } else {
                thumbImageCarouselWap.removeClass("visibility-hidden");
            }
        } else {
            this.diffSelectionEventproductImageViewPopulateNewImages(o, parentImagesChildren, thumbImageCarouselChildren, alternativeText, parentImages, thumbImageCarousel);
            this.diffSelectionEventproductImageViewCreateCarousels(o, parentImagesChildren, thumbImageCarouselChildren, alternativeText, false, parentImages, thumbImageCarousel);
            thumbImageCarouselWap.addClass("visibility-hidden");
        }
    }

    returnEComCodeSellOnlineType(v, t) {
        if (t === "onlineInventory") {
            switch (v) {
                case '01': // Sell Online
                    return true;
                    break;
                case '02': // Display Only
                    return false;
                    break;
                case '03': // No Display
                    return true;
                    break;
                case '04': // Drop Ship
                    return true;
                    break;
                case '05': // Recall
                    return true;
                    break;
                case '06': // Online Only
                    return true;
                    break;
            }
        } else if (t === "inStoreInventory") {
            switch (v) {
                case '01': // Sell Online
                    return true;
                    break;
                case '02': // Display Only
                    return true;
                    break;
                case '03': // No Display
                    return false;
                    break;
                case '04': // Drop Ship
                    return false;
                    break;
                case '05': // Recall
                    return true;
                    break;
                case '06': // Online Only
                    return false;
                    break;
            }
        }
    }

    diffSelectionEventStaticInventoryStatusLsi(lisfisi, lisfis, listc, eComCodeSellInStoreStatic, lsiButtons, o) {
        // yes, initiate LSI and pass required information
        if (eComCodeSellInStoreStatic === true) {
            // check if its a clearance item
            if (this.currentAdBugType != "adbug-clearance") {
                // check if store is setup
                storeLocatorLSI.currentSKU = o["partNumber"];
                if ( window.getCookie("WC_StLocId") === undefined ) {
                    lsiButtons.removeClass("hide");
                    let soldInStoresMessage =
                        `
                        <i class="icon-local-inventory-available">
                            <svg class="icon"><use xlink:href="#checkCircle"></use>
                            </svg>
                        </i>
                        <h3 class="store-availability-message clearfix">Sold in Stores</h3>
                        `;
                    listc.html(soldInStoresMessage);
                }
                if(this.localStoreInventoryInit == false && window.getCookie("WC_StLocId")) {
                    // call common LSI call
                    storeLocatorLSI.placeInventoryInformationOnPDP();
                    storeLocatorLSI.updateModalOnSkuChange();
                } else {
                    storeLocatorLSI.init();
                    this.localStoreInventoryInit = false;
                }
            }
            else {
                lsiButtons.addClass("hide");
                let noLookupMessaging =
                    `
                    Store Inventory Lookup Not Available
                    `;
                listc.html(noLookupMessaging);
            }
        }
        else {
            lsiButtons.addClass("hide");
            let notSoldInStoresMessage =
                `
                    <i class="icon-local-inventory-unavailable">
                        <svg class="icon"><use xlink:href="#times-circle"></use>
                        </svg>
                    </i>
                    <h3 class="store-availability-message clearfix">Not Sold In Stores</h3>
                    `;
            listc.html(notSoldInStoresMessage);

        }
    }

    diffSelectionEventStaticInventoryStatus(o) {
        // get the ecom code
        var diffInventoryStatusNode = document.querySelector(`#diff-inventory-status-${this.catalogEntryID}`);
        var inventoryQuantity = parseInt(o.inventoryQty);
        var eComCodeValue = o.ecomCodeDesc.substring(0, 2);
        var eComCodeSellOnline = this.returnEComCodeSellOnlineType(eComCodeValue, o.onlineInventory.title);
        var eComCodeSellInStoreStatic = this.returnEComCodeSellOnlineType(eComCodeValue, o.inStoreInventory.title);

        // check if available to sell online
        if (eComCodeSellOnline === true) {
            if (inventoryQuantity > 0) {
                let inStockMessage =
                    `<div>
                        <i class="icon-local-inventory-available">
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkCircle"></use>
                            </svg>
                        </i>
                    </div>
                    <div>
                        <h3>In Stock Online</h3> <span>Usually leaves warehouse in 1-2 business days.</span>
                    </div>`;
                let inStockMessageNode = document.querySelector(`#InventoryStatus_Online_Heading_${this.catalogEntryID}`);
                inStockMessageNode.innerHTML = inStockMessage;
                this.diffSelectionEventsAddToCartStatus("enable", "ISO");
            } else {
                let outOfStockMessage =
                    `<div>
                        <i class="icon-local-inventory-unavailable">
                            <svg class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#times-circle"></use>
                            </svg>
                        </i>
                    </div>
                    <div>
                        <h3>Out of Stock Online</h3>
                    </div>`;
                let outOfStockMessageNode = document.querySelector(`#InventoryStatus_Online_Heading_${this.catalogEntryID}`);
                outOfStockMessageNode.innerHTML = outOfStockMessage;
                this.diffSelectionEventsAddToCartStatus("disable", "OSO");
            }
        } else {
            // no, set online messaging and disable add to cart
            let notSoldOnlineMessage =
                `<div>
                    <i class="icon-local-inventory-unavailable">
                        <svg class="icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#times-circle"></use>
                        </svg>
                    </i>
                </div>
                <div>
                    <h3>Not Sold Online</h3>
                </div>`;
            let notSoldOnlineMessageNode = document.querySelector(`#InventoryStatus_Online_Heading_${this.catalogEntryID}`);
            notSoldOnlineMessageNode.innerHTML = notSoldOnlineMessage;

            this.diffSelectionEventsAddToCartStatus("disable", "NSO");
        }

        if (diffInventoryStatusNode != null) {
            GeneralComponentsClass.removeClass(diffInventoryStatusNode, "visibility-hidden");
        }

        // check if LSI is available
        // #local-inventory-section-find-in-store-init or #local-inventory-section-find-in-store
        var lisfisi = jQuery('#local-inventory-section-find-in-store-init'),
            lisfis = jQuery('#local-inventory-section-find-in-store'),
            listc = jQuery(".local-inventory-section-title-content"),
            lsiButtons = jQuery(".local-inventory-section-buttons"),
            safetyStockEnabled = jQuery("#aySafetyStockEnabled"),
            safetyStockEnabledValue = safetyStockEnabled.val(),
            storeDetailsInventory = jQuery(".store-details-inventory");
        if (lisfisi === null || lisfis === null) {
            // lsi not available, check if "sell in store" for static inventory message

            // yes, set static messaging
            // no, set statuc messaging
        } else {
            // check for safety stock
            if (safetyStockEnabled.length) {
                if (safetyStockEnabledValue !== null && safetyStockEnabledValue === "N") {
                    listc.html("Store Inventory Lookup Not Available");
                    lsiButtons.addClass("hide");
                    storeDetailsInventory.html("");
                    return;
                }
                else {
                    this.diffSelectionEventStaticInventoryStatusLsi(lisfisi, lisfis, listc, eComCodeSellInStoreStatic, lsiButtons, o);
                }
            } else {
                this.diffSelectionEventStaticInventoryStatusLsi(lisfisi, lisfis, listc, eComCodeSellInStoreStatic, lsiButtons, o);
            }
        }

    }

    disableInvodoSpin() {
        let invodoSpinActive = jQuery("#spin-wrapper").hasClass("novisibility-hidden");
        if (!invodoSpinActive) {
            window.academyInvodo.spinWrapper.classList.add("novisibility-hidden");
            jQuery("#invodo360").removeClass("active");
            window.academyInvodo.invodoSpinItem.zoomOut();
            window.academyInvodo.invodoSpinItem.home();
        }
    }

    diffSelectionEventsAddToCartStatus(t, s) {
        var addToCartButton = jQuery("#add2CartBtn");
        var addToCartQtyInput = jQuery(`#quantity_${this.catalogEntryID}`);
        var addToCartSection = jQuery("#rpdp-product-add-to-cart");
        var wishListButton = jQuery("#rpdp-product-wishlist");
        if (t === "enable") {
            if (addToCartButton != null) {
                addToCartButton.removeClass("disabled");
                addToCartQtyInput.removeClass("disabled");
                addToCartButton.prop("disabled", false);
                addToCartQtyInput.prop("disabled", false);
                addToCartSection.removeClass("visibility-hidden");
            }
        } else {
            if (addToCartButton != null) {
                addToCartButton.addClass('disabled');
                addToCartQtyInput.addClass('disabled');
                addToCartButton.prop("disabled", true);
                addToCartQtyInput.prop("disabled", true);
                addToCartSection.removeClass("visibility-hidden");
            }
        }
        if (wishListButton != null) {
            wishListButton.removeClass("visibility-hidden");
        }
        var spinaroo = jQuery('#add2CartBtn').children();
        switch (s) {
            case "OSO":
                jQuery('#add2CartBtn').text('Out of Stock').append(spinaroo);
                break;
            case "ISO":
                jQuery('#add2CartBtn').text('Add to Cart').append(spinaroo);
                break;
            case "NSO":
                jQuery('#add2CartBtn').text('Not Sold Online').append(spinaroo);
                break;
            default:
                jQuery('#add2CartBtn').text('Add to Cart').append(spinaroo);
                break;
        }
    }

    buildDiffCombinationFromList() {
        var obj = {};
        var objCombo = this.skuDiffCombinations;
        var combinationFromList = "pdp_product-";
        var al = document.querySelectorAll('.atrribute-list');
        for (var i = 0; i < al.length; i++) {
            var checkedElement = al[i].getElementsByClassName('checked');
            var dataId = checkedElement[0].getAttribute("data-id");
            combinationFromList += dataId;
            if (i < (al.length - 1))
                combinationFromList += "-";
        }
        return objCombo[combinationFromList];
    }

    diffSelectionEventDiffSelectionEvents() {
        jQuery(".pdp-diff-item").on("click", function (e) {
            e.preventDefault();
            PdpDiffSelection.disableInvodoSpin();
            var currentActiveNode = jQuery(this).parent().parent().find(".checked");
            currentActiveNode.removeClass("checked");
            var futureNode = jQuery(this);
            futureNode.addClass("checked");
            var currentDiffCombinationObj = PdpDiffSelection.buildDiffCombinationFromList();
            if (currentDiffCombinationObj != undefined) {
                PdpDiffSelection.productExists = true;
                //set hash on window to catentry
                history.replaceState(undefined, undefined, `#repChildCatSku=${currentDiffCombinationObj["partNumber"]}`);
                PdpDiffSelection.currentPartNumber = currentDiffCombinationObj["partNumber"];
                PdpDiffSelection.currentCatEntryId = currentDiffCombinationObj["catentry_id"];
                PdpDiffSelection.currentOfferPrice = currentDiffCombinationObj["offerPrice"];
                PdpDiffSelection.diffSelectionEvents(currentDiffCombinationObj);
            } else {
                PdpDiffSelection.productExists = false;
                let firstAttributeList = jQuery(".atrribute-list:first").find(".checked");
                let imgUrl = firstAttributeList.find("img").attr("src").replace("?is=150,150", "");
                let obj = {};
                obj["no-sku"] = "true";
                obj["default-image"] = imgUrl;
                obj["ecomCodeDesc"] = "02 Display Only";
                obj["inventoryQty"] = "0";
                obj["Attributes"] = {};
                obj["onlineInventory"] = {"title": "inStoreInventory"};
                obj["inStoreInventory"] = {"title": "onlineInventory"};
                PdpDiffSelection.diffSelectionEvents(obj);
            }
        });
    }

    diffSelectionEventsShowProductInfo() {
        let showProductMainInfo = document.querySelector("#rpdp-product-diffs-pricing-diffs"),
            ProductReviewsQa = document.querySelector("#rpdp-product-reviews-qa"),
            ProductIdSKu = document.querySelector("#rpdp-product-id");
        GeneralComponentsClass.removeClass(showProductMainInfo, "visibility-hidden");
        GeneralComponentsClass.removeClass(ProductReviewsQa, "visibility-hidden");
        GeneralComponentsClass.removeClass(ProductIdSKu, "visibility-hidden");
    }

    diffSelectionEvents(o) {
        // create key array from "o" Attributes for Diff Group on DOM
        var diffAttributeKeyArr = Object.keys(o.Attributes);

        if (o["no-sku"] == undefined) {
            // select diffs on DOM
            this.diffSelectionEventsChecked(diffAttributeKeyArr, o);
            // hide if single diff
            this.diffSelectionEventsCheckAttributeListTotal();
            // change pricing if different
            this.diffSelectionEventsPricing(o);
            // change sku and item number
            this.diffSelectionEventsChangeSkuItemNumbers(o);
            // update product manufacturing part number
            this.diffSelectionEventsChangeManufacturingPartNumber(o);
            // activate adbugs if they are available
            this.diffSelectionEventsAdBugAvailability(o);
            // replace the product image views if
            this.diffSelectionEventProductImageView(o);
            // update static inventory status
            this.diffSelectionEventStaticInventoryStatus(o);
            // reveal product information and hide prespinner
            this.diffSelectionEventsShowProductInfo();
        } else {
            this.diffSelectionEventsChecked(diffAttributeKeyArr, o);
            // hide if single diff
            this.diffSelectionEventsCheckAttributeListTotal();
            // change sku and item number
            this.diffSelectionEventsChangeSkuItemNumbers(o);
            // update product manufacturing part number
            this.diffSelectionEventsChangeManufacturingPartNumber(o);
            // activate adbugs if they are available
            this.diffSelectionEventsAdBugAvailability(o);
            // replace the product image views if
            this.diffSelectionEventProductImageView(o);
            // update static inventory status
            this.diffSelectionEventStaticInventoryStatus(o);
        }
    }

    hashTypeIsAvailable(t) {
        var _this = t;
        // check if Diff combination is available via
        // data path this.hashKey and this.hashIdLocateDiffCombination
        var diffComboCheckPreference = _this.productJsonView[_this.hashKey][_this.hashIdLocateDiffCombination];
        if (typeof diffComboCheckPreference != 'undefined') {
            // set the diff combination reference
            let diffCombination = diffComboCheckPreference["diff-combination"];
            // check to see if its available as part of DiffCombination List
            let diffCombinationInformation = _this.skuDiffCombinations[`pdp_product-${diffCombination}`];
            if (typeof diffCombinationInformation === 'undefined') {
                _this.hashTypeNotAvailable(_this);
            } else {
                _this.currentPartNumber = diffCombinationInformation["partNumber"];
                _this.currentCatEntryId = diffCombinationInformation["catentry_id"];
                _this.currentOfferPrice = diffCombinationInformation["offerPrice"];
                _this.diffSelectionEvents(diffCombinationInformation);
            }
        } else {
            _this.hashTypeNotAvailable(_this);
        }
    }

    hashTypeNotAvailable(t) {
        var _this = t;
        // select the first available combination by finding
        // the first key in this.skuDiffCombination
        let diffCombinationInformation = _this.skuDiffCombinations[Object.keys(_this.skuDiffCombinations)[0]];
        _this.currentPartNumber = diffCombinationInformation["partNumber"];
        _this.currentCatEntryId = diffCombinationInformation["catentry_id"];
        _this.currentOfferPrice = diffCombinationInformation["offerPrice"];
        _this.diffSelectionEvents(diffCombinationInformation);
    }

    successProductJsonViewLoad(t, r) {
        // set product data
        var _this = t;
        _this.productJsonView = r[`pdp_product-${_this.catalogEntryID}`];
        _this.skuDiffCombinations = _this.productJsonView["sku-diff_combinations"];

        //  check if there is a hash property
        if (_this.hashType != "noPreference" && _this.hashIdLocateDiffCombination != "") {
            _this.hashTypeIsAvailable(_this);
        } else {
            _this.hashTypeNotAvailable(_this);
        }
    }

    errorProductJsonViewLoad() {
        MessageHelper.displayErrorMessage(storeNLS["PRODUCT_JSON_RETRIEVAL_ERROR"]);
    }

    loadProductJsonView() {
        GeneralComponentsClass.getAjax(`${GeneralComponentsClass.returnAbsoluteURL()}AYRProductJsonView?productId=${this.catalogEntryID}`, this.successProductJsonViewLoad, this.errorProductJsonViewLoad, this);
    }

    initRenderProductDisplayPageInformation() {
        var getProductDiffSelectable = this.diffProductType;
        // set the catalog entry ID
        if (this.catalogEntryID != null) {
            this.catalogEntryID = this.catalogEntryID.getAttribute("data-id");
        } else {
            return;
        }

        switch (getProductDiffSelectable) {
            case 'not-selectable': // meaning there are no diffs to select
                this.selectableProduct = false;
                this.whichTypeofHashAreYou();
                this.loadProductJsonView();
                break;
            case 'selectable': // there is atleast one diff that is selectable
                this.selectableProduct = true;
                this.diffSelectionEventDiffSelectionEvents();
                this.whichTypeofHashAreYou();
                this.loadProductJsonView();
                break;
        }

    }
}

if (GeneralComponentsClass.pageName === "ProductPage") {
    // check you are not gift card
    var giftCardCheck = jQuery(".definingAttributes").hasClass("gift-card");
    if (!giftCardCheck) {
        var PdpDiffSelection = new pdpDiffSelection();
        PdpDiffSelection.initRenderProductDisplayPageInformation();
    }
}